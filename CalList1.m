function list = CalList()

%FOVEAL ATTENTION
% stimulus duration: stimOff-saccOn.
% this is because the stimulus duration 
% is calculated from the moment the eye lands.

list = eis_readData([], 'x');
list = eis_readData(list, 'y');
list = eis_readData(list, 'trigger', 'blink');
list = eis_readData(list, 'trigger', 'notrack');
list = eis_readData(list, 'stream', 0, 'double');
list = eis_readData(list, 'stream', 1, 'double');
%list = eis_readData(list, 'joypad', 'r1'); %commented this out
list = eis_readData(list, 'trigger', 'frame');
list = eis_readData(list, 'photocell'); % photocell

% % user variables
list = eis_readData(list, 'uservar', 'Subject_Name');

list = eis_readData(list, 'uservar', 'Response');
list = eis_readData(list, 'uservar', 'Correct');

list = eis_readData(list, 'uservar', 'TimeCueON');
list = eis_readData(list, 'uservar', 'TimeCueOFF');
list = eis_readData(list, 'uservar', 'TimeFixationON');
list = eis_readData(list, 'uservar', 'TimeFixationOFF');
list = eis_readData(list, 'uservar', 'TimeTargetON');
list = eis_readData(list, 'uservar', 'TimeTargetOFF');
list = eis_readData(list, 'uservar', 'CueTargetTiming');
list = eis_readData(list, 'uservar', 'ResponseTime');
list = eis_readData(list, 'uservar', 'TimePesponseCueOFF');

list = eis_readData(list, 'uservar', 'xoffset'); %px
list = eis_readData(list, 'uservar', 'yoffset'); %px
list = eis_readData(list, 'uservar', 'debug');

list = eis_readData(list, 'uservar', 'X_TargetOffset');
list = eis_readData(list, 'uservar', 'Y_TargetOffset');
list = eis_readData(list, 'uservar', 'TestCalibration');

list = eis_readData(list, 'uservar', 'TrialType');
%list = eis_readData(list, 'uservar', 'TargetPosition');
list = eis_readData(list, 'uservar', 'unstab');
list = eis_readData(list, 'uservar', 'RGB_value');

list = eis_readData(list, 'uservar', 'PreTargetCueLocation');
list = eis_readData(list, 'uservar', 'ResponseCueLocation');

list = eis_readData(list, 'uservar', 'Target1_Orientation');
list = eis_readData(list, 'uservar', 'Target2_Orientation');
list = eis_readData(list, 'uservar', 'Target3_Orientation');
list = eis_readData(list, 'uservar', 'Target4_Orientation');

list = eis_readData(list, 'uservar', 'Fixed_RC_Off');
list = eis_readData(list, 'uservar', 'fovea');




        



