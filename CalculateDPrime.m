function [d std_d ci_d crit] = CalculateDPrime(Response, TargetOrientation)

Hi = length( find(TargetOrientation == 1 & Response == 1) ); %hit rate
Fa = length( find(TargetOrientation == 0 & Response == 1) ); %false alarm
Mi = length( find(TargetOrientation == 1 & Response == 0) ); %miss rate
Cr = length( find(TargetOrientation == 0 & Response == 0) ); %correct rejection


HitRate = Hi/(Hi+Mi);
FaRate = Fa/(Cr+Fa);


% apply correction to have bounds to dprime
[HitRate FaRate] = CorrectionDPrime(HitRate, FaRate, Hi, Mi, Fa, Cr);

d = norminv(HitRate)-norminv(FaRate);

% % percent 
% PercentMiss_i = Mi/(Mi+Hi+Fa+Cr);
% PercentFA_i = Fa/(Mi+Hi+Fa+Cr);
% PercentMiss_v = Mv/(Mv+Hv+Fa+Cr);
% PercentFA_v = Fa/(Mv+Hv+Fa+Cr);
% PercentMiss_n = Mn/(Mn+Hn+Fa_n+Cr_n);
% PercentFA_n = Fa_n/(Mn+Hn+Fa_n+Cr_n);

% standard deviation
% from Mcmillian and Creelman - detection theory pg 325-327
phi_h = (2 * pi)^-.50 * exp(-.5 * ((sqrt(2) * erfinv(2 * HitRate - 1))^2));
phi_f = (2 * pi)^-.50 * exp(-.5 * ((sqrt(2) * erfinv(2 * FaRate - 1))^2));
var_d = (((HitRate * (1 - HitRate))/((Hi+Mi) * ((phi_h)^2))) + ...
    ((FaRate * (1 - FaRate))/((Cr+Fa) * ((phi_f)^2))));
std_d = sqrt(var_d);
ci_d = std_d * 1.96;

% bias/criterion
crit = -([norminv(HitRate)+norminv(FaRate)])/2;

function [HitRate FalseAlarmRate] = CorrectionDPrime(HitRate, FalseAlarmRate, H, M, F, C);
% correction approach see stanislaw and Todorow 99

if (HitRate == 0)
    %HitRate = (1/(2*(H+M)));
    HitRate = 0.5/(H+M);
end
if (FalseAlarmRate == 0)
    %FalseAlarmRate = (1/(2*(F+C)));
    FalseAlarmRate = 0.5/(F+C);
end
if (HitRate == 1)
    %HitRate = (1-(1/(2*(H+M))));
    HitRate = 1-(0.5/(H+M));
end
if (FalseAlarmRate == 1)
    %FalseAlarmRate = (1-(1/(2*(F+C))));
    FalseAlarmRate = 1-(0.5/(F+C));
end

