clear all

control = 'periphery'; %fovea, periphery
subject = 'Adriana'; %Adriana, Karina, Anna, Giorgio
single = 1;
includems = 0; %do not change

for mm = 1:2

    if (strcmp(control, 'periphery') && (includems == 1))
        %if strcmp(subject, 'Adriana')
            Adriana_error = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDrift/performance_error_array.mat');
            Adriana_x = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDrift/performance_x_array.mat');
            Adriana_y = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDrift/performance_y_array.mat');
        %elseif strcmp(subject, 'Anna')
            Anna_error = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDrift/performance_error_array.mat');
            Anna_x = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDrift/performance_x_array.mat');
            Anna_y = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDrift/performance_y_array.mat');
        %elseif strcmp(subject, 'Karina')
            Karina_error = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDrift/performance_error_array.mat');
            Karina_x = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDrift/performance_x_array.mat');
            Karina_y = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDrift/performance_y_array.mat');
        %end
    elseif (strcmp(control, 'periphery') && (includems == 0))
        %if strcmp(subject, 'Adriana')
            Adriana_error = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/Drift/performance_error_array.mat');
            Adriana_x = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/Drift/performance_x_array.mat');
            Adriana_y = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/Drift/performance_y_array.mat');
        %elseif strcmp(subject, 'Anna')
            Anna_error = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/Drift/performance_error_array.mat');
            Anna_x = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/Drift/performance_x_array.mat');
            Anna_y = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/Drift/performance_y_array.mat');
        %elseif strcmp(subject, 'Karina')
            Karina_error = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/Drift/performance_error_array.mat');
            Karina_x = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/Drift/performance_x_array.mat');
            Karina_y = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/Drift/performance_y_array.mat');
        %end
    elseif (strcmp(control, 'fovea') && (includems == 1))
        %if strcmp(subject, 'Giorgio')
            Giorgio_error = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/MsDrift/performance_error_array.mat');
            Giorgio_x = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/MsDrift/performance_x_array.mat');
            Giorgio_y = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/MsDrift/performance_y_array.mat');
        %elseif strcmp(subject, 'Adriana')
            Adriana_error = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDrift/performance_error_array.mat');
            Adriana_x = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDrift/performance_x_array.mat');
            Adriana_y = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDrift/performance_y_array.mat');  
        %elseif strcmp(subject, 'Anna')
            Anna_error = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDrift/performance_error_array.mat');
            Anna_x = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDrift/performance_x_array.mat');
            Anna_y = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDrift/performance_y_array.mat');  
        %elseif strcmp(subject, 'Karina')
            Karina_error = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/fovea/MsDrift/performance_error_array.mat');
            Karina_x = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/fovea/MsDrift/performance_x_array.mat');
            Karina_y = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/fovea/MsDrift/performance_y_array.mat');    
        %end
    elseif (strcmp(control, 'fovea') && (includems == 0))
        %if strcmp(subject, 'Giorgio')
            Giorgio_error = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/Drift/performance_error_array.mat');
            Giorgio_x = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/Drift/performance_x_array.mat');
            Giorgio_y = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/Drift/performance_y_array.mat');
        %elseif strcmp(subject, 'Adriana')
            Adriana_error = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/Drift/performance_error_array.mat');
            Adriana_x = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/Drift/performance_x_array.mat');
            Adriana_y = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/Drift/performance_y_array.mat');  
        %elseif strcmp(subject, 'Anna')
            Anna_error = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/Drift/performance_error_array.mat');
            Anna_x = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/Drift/performance_x_array.mat');
            Anna_y = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/Drift/performance_y_array.mat');  
        %elseif strcmp(subject, 'Karina')
            Karina_error = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/fovea/Drift/performance_error_array.mat');
            Karina_x = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/fovea/Drift/performance_x_array.mat');
            Karina_y = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/fovea/Drift/performance_y_array.mat');    
        %end
    end


    %%
    % combine performance across ISIs (per subject, ms/driftonly,
    % periphery/fovea)
    
    error_array = eval(strcat(subject, '_error'));
    error_array = error_array.error_array;
    xarray = eval(strcat(subject, '_x'));
    xarray = xarray.xarray;
    yarray = eval(strcat(subject, '_y'));
    yarray = yarray.yarray;


    %%
    %xarray = strcat(subject, '_x');
    %yarray = strcat(subject, '_y');

    xarray = xarray';
    xarray= xarray(1, :); %each ISI)

    total_performance = sum(yarray')';
    total_error = sum(error_array')';
    total_trialtypes = 3; %valid, invalid, neutral

    performance = total_performance/total_trialtypes;
    error = total_error/total_trialtypes;

    %line(xarray,performance)
    errorbar(xarray,performance,error, 'LineWidth',2)
    hold on
    %loglog(xarray,yarray,'k+','LineWidth',2);
    ylabel('d_prime');
    xlabel('ISI Level');
    legend('Drift','Drift & MS','Location','southeast','Orientation','horizontal');
    set(gca,'fontsize',20)
    set(findall(gca, 'Type', 'Line'),'LineWidth',2);
    xticks(xarray)
    xlim([-10 775])
    if includems == 1
        t = strcat(subject,'_Subject_Final_Sensitivity_Plot_(ms_drift)');
        title(strcat(subject,' Sensitivity (ms & drift)'));
    else
        t = strcat(subject,'_Subject_Final_Sensitivity_Plot_(drift_only)');
        title(strcat(subject,' Sensitivity (drift only)'));
    end

    saveas(gca,sprintf(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', subject,'/Images/', control, '/', 'General/',t)));
    saveas(gca,sprintf(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', subject,'/Images/', control, '/', 'General/',t,'.png')));

    includems = includems+1;
       
    
end