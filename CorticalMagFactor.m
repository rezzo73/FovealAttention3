% foveal magnification factor based on pg 67 of Carrasco Frieder 1997 see
% also StrasbugerEtAl94
function M = CorticalMagFactor(Ecc)

% size of the small stimulus on the x axis 0.0833 deg
% size of the large stimulus on the x axis 0.1667 deg

% Lets consider the cortical
% magnification in the fovea M_o=7.99 (this should be mm/deg), 
% this means that if our target is 0.0833 deg in size on the cortical 
% surface it will cover a region of M mm/deg*0.0833 deg = 0.6656 mm. For our
% large stimulus (0.1667 deg) at 3 deg M = 3.5317 mm/deg, therefore its
% covered cortical region is M*0.1667 = 0.5887 mm.
% the size of the stimulus in deg is given by S_o * M_o/M where S_o is the
% size of the stimulus at the fovea in mm and M is the magnification
% factor at a given eccentricity. Here we have 0.0833 deg * (7.99mmdeg/3.5317mmdeg) =
% 0.1885 deg (11 arcmin).

% based on the cortical surface covered:
% the foveal target covers 7.99 mm/deg * 0.0833 deg = 0.6656 mm, 
% and the target in the parafovea covers 3.5317 mm/deg * 0.1667 = 0.5887 mm.
% Considering that the magnification factor is approximate, I think a difference 
% of 0.0769 mm (considering the full area covered by the square targets we have 
% 0.49 mm^2 ? 0.36 mm^2 = 0.13 mm^2) 

% ecc in deg M is in mm per deg
% if eccentricity is smaller than the fovea than M = 7.99
if Ecc>1
    M_0 = 7.99; %mm/deg based on Virsu and rovamo
    M_sup = M_0*(1+0.42*Ecc+0.00012*Ecc^3)^-1;
    M_inf = M_0*(1+0.42*Ecc+0.000055*Ecc^3)^-1;
    M = mean([M_sup M_inf]);
else
    M = 7.99;
end