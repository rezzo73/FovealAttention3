

subject = 'Anna'; % [ Anna, Adriana, Karina]
control = 'periphery';

if strcmp(subject, 'Adriana')
    ISI450 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/MSLanding2Cue_ISI450.mat');
    ISI750 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/MSLanding2Cue_ISI750.mat');
elseif strcmp(subject, 'Anna')
    ISI450 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/MSLanding2Cue_ISI450.mat');
    ISI750 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/MSLanding2Cue_ISI750.mat');    
elseif strcmp(subject, 'Karina')
    ISI450 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/MSLanding2Cue_ISI450.mat');
    ISI750 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/MSLanding2Cue_ISI750.mat');
end

f=figure;
Name={'A','B','C'};
data=[round(ISI450.MSLanding2Cue.MS_cue_left*100,1) round(ISI450.MSLanding2Cue.MS_cue_right*100,1);...
    round(ISI750.MSLanding2Cue.MS_cue_left*100,1) round(ISI750.MSLanding2Cue.MS_cue_right*100,1)];
%h = uitable(f,'Data',data, 'ColumnName',headers);
headers = ['ISI_450'; 'ISI_750'];
t = uitable('InnerPosition',[5 5 295 56],'RowName',{'% MS&Cue_Left';'% MS&Cue_Right'},'ColumnName',headers,'Data',data);
handles.uitable1.Position = [5 5 295 56];

title = 'table_MS_cue_performance';

saveas(gca,sprintf(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', subject,'/Images/', control, '/', 'General/',title)));
saveas(gca,sprintf(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', subject,'/Images/', control, '/', 'General/',title,'.png')));