% dC1 = d prime of condition 1
% dC2 = d prime of condition 2
% varC1 = variance for condition 1
% varC2 = variance for condition 2
% see CalculateD 
% compare d prime pg(328 MacMillian)

function  [h] = DPrimeTest(dC1,varC1,dC2,varC2)

d_diff = abs((dC1-dC2));
ci_diff = sqrt(varC1+varC2)*1.96;
if (d_diff+ci_diff)>0 & (d_diff-ci_diff)>0
    h = 1;
else
    h = 0;
end