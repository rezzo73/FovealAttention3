% Combining ms frequency graphs (periphery)
clear all

control = 'fovea'; %fovea, periphery
subject = 'Giorgio'; %['Adriana', 'Anna', 'Karina', 'Giorgio'];

if strcmp(control, 'periphery')
        ISI5_0 = load(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/',subject,'/Data/periphery/MsDetails/MSDetails_ISI5TrialType0.mat'));
        ISI5_1 = load(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/',subject,'/Data/periphery/MsDetails/MSDetails_ISI5TrialType1.mat'));
        ISI5_2 = load(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/',subject,'/Data/periphery/MsDetails/MSDetails_ISI5TrialType2.mat'));
        ISI60_0 = load(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/',subject,'/Data/periphery/MsDetails/MSDetails_ISI60TrialType0.mat'));
        ISI60_1 = load(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/',subject,'/Data/periphery/MsDetails/MSDetails_ISI60TrialType1.mat'));
        ISI60_2 = load(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/',subject,'/Data/periphery/MsDetails/MSDetails_ISI60TrialType2.mat'));
        ISI450_0 = load(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/',subject,'/Data/periphery/MsDetails/MSDetails_ISI450TrialType0.mat'));
        ISI450_1 = load(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/',subject,'/Data/periphery/MsDetails/MSDetails_ISI450TrialType1.mat'));
        ISI450_2 = load(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/',subject,'/Data/periphery/MsDetails/MSDetails_ISI450TrialType2.mat'));
        ISI750_0 = load(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/',subject,'/Data/periphery/MsDetails/MSDetails_ISI750TrialType0.mat'));
        ISI750_1 = load(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/',subject,'/Data/periphery/MsDetails/MSDetails_ISI750TrialType1.mat'));
        ISI750_2 = load(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/',subject,'/Data/periphery/MsDetails/MSDetails_ISI750TrialType2.mat'));
elseif strcmp(control, 'fovea')
    if strcmp(subject, 'Giorgio')
        ISI10_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/MsDetails/MSDetails_ISI10TrialType0.mat');
        ISI10_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/MsDetails/MSDetails_ISI10TrialType1.mat');
        ISI10_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/MsDetails/MSDetails_ISI10TrialType2.mat');
        ISI60_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/MsDetails/MSDetails_ISI60TrialType0.mat');
        ISI60_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/MsDetails/MSDetails_ISI60TrialType1.mat');
        ISI60_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/MsDetails/MSDetails_ISI60TrialType2.mat');
        ISI150_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/MsDetails/MSDetails_ISI150TrialType0.mat');
        ISI150_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/MsDetails/MSDetails_ISI150TrialType1.mat');
        ISI150_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/MsDetails/MSDetails_ISI150TrialType2.mat');
        ISI200_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/MsDetails/MSDetails_ISI200TrialType0.mat');
        ISI200_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/MsDetails/MSDetails_ISI200TrialType1.mat');
        ISI200_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/MsDetails/MSDetails_ISI200TrialType2.mat');
        ISI450_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/MsDetails/MSDetails_ISI450TrialType0.mat');
        ISI450_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/MsDetails/MSDetails_ISI450TrialType1.mat');
        ISI450_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Giorgio/Data/fovea/MsDetails/MSDetails_ISI450TrialType2.mat');        
    elseif strcmp(subject, 'Adriana')
        ISI5_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDetails/MSDetails_ISI5TrialType0.mat');
        ISI5_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDetails/MSDetails_ISI5TrialType1.mat');
        ISI5_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDetails/MSDetails_ISI5TrialType2.mat');
        ISI10_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDetails/MSDetails_ISI10TrialType0.mat');
        ISI10_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDetails/MSDetails_ISI10TrialType1.mat');
        ISI10_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDetails/MSDetails_ISI10TrialType2.mat');
        ISI450_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDetails/MSDetails_ISI450TrialType0.mat');
        ISI450_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDetails/MSDetails_ISI450TrialType1.mat');
        ISI450_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDetails/MSDetails_ISI450TrialType2.mat');
        ISI600_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDetails/MSDetails_ISI600TrialType0.mat');
        ISI600_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDetails/MSDetails_ISI600TrialType1.mat');
        ISI600_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDetails/MSDetails_ISI600TrialType2.mat');
        ISI750_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDetails/MSDetails_ISI750TrialType0.mat');
        ISI750_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDetails/MSDetails_ISI750TrialType1.mat');
        ISI750_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDetails/MSDetails_ISI750TrialType2.mat');
    elseif strcmp(subject, 'Anna')
        ISI5_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDetails/MSDetails_ISI5TrialType0.mat');
        ISI5_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDetails/MSDetails_ISI5TrialType1.mat');
        ISI5_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDetails/MSDetails_ISI5TrialType2.mat');
        ISI10_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDetails/MSDetails_ISI10TrialType0.mat');
        ISI10_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDetails/MSDetails_ISI10TrialType1.mat');
        ISI10_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDetails/MSDetails_ISI10TrialType2.mat');
        ISI60_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDetails/MSDetails_ISI60TrialType0.mat');
        ISI60_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDetails/MSDetails_ISI60TrialType1.mat');
        ISI60_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDetails/MSDetails_ISI60TrialType2.mat');
        ISI150_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDetails/MSDetails_ISI150TrialType0.mat');
        ISI150_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDetails/MSDetails_ISI150TrialType1.mat');
        ISI150_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDetails/MSDetails_ISI150TrialType2.mat');
        ISI200_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDetails/MSDetails_ISI200TrialType0.mat');
        ISI200_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDetails/MSDetails_ISI200TrialType1.mat');
        ISI200_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDetails/MSDetails_ISI200TrialType2.mat');
        ISI450_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDetails/MSDetails_ISI450TrialType0.mat');
        ISI450_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDetails/MSDetails_ISI450TrialType1.mat');
        ISI450_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/fovea/MsDetails/MSDetails_ISI450TrialType2.mat');
    elseif strcmp(subject, 'Karina')
        ISI5_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/fovea/MsDetails/MSDetails_ISI5TrialType0.mat');
        ISI5_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/fovea/MsDetails/MSDetails_ISI5TrialType1.mat');
        ISI5_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/fovea/MsDetails/MSDetails_ISI5TrialType2.mat');
        ISI60_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/fovea/MsDetails/MSDetails_ISI60TrialType0.mat');
        % where are trials for 60_1 or 60_2?
        ISI450_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/fovea/MsDetails/MSDetails_ISI450TrialType0.mat');
        ISI450_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/fovea/MsDetails/MSDetails_ISI450TrialType1.mat');
        ISI450_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/fovea/MsDetails/MSDetails_ISI450TrialType2.mat');
    end
end

%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmp(control, 'periphery')
       ISI5_DSPAN = mean([ISI5_0.TrialDetails.Driftspan, ISI5_1.TrialDetails.Driftspan, ISI5_2.TrialDetails.Driftspan]);
       ISI5_DSPAN_SEM = mean([ISI5_0.TrialDetails.Driftspan_SEM, ISI5_1.TrialDetails.Driftspan_SEM, ISI5_2.TrialDetails.Driftspan_SEM]);
       ISI5_DSPEED = mean([ISI5_0.TrialDetails.Driftspeed, ISI5_1.TrialDetails.Driftspeed, ISI5_2.TrialDetails.Driftspeed]);
       ISI5_DSPEED_SEM = mean([ISI5_0.TrialDetails.Driftspeed_SEM, ISI5_1.TrialDetails.Driftspeed_SEM, ISI5_2.TrialDetails.Driftspeed_SEM]); 
       ISI60_DSPAN = mean([ISI60_0.TrialDetails.Driftspan]);
       ISI60_DSPAN_SEM = mean([ISI60_0.TrialDetails.Driftspan_SEM]);
       ISI60_DSPEED = mean([ISI60_0.TrialDetails.Driftspeed]);
       ISI60_DSPEED_SEM = mean([ISI60_0.TrialDetails.Driftspeed_SEM]);
       ISI450_DSPAN = mean([ISI450_0.TrialDetails.Driftspan, ISI450_1.TrialDetails.Driftspan, ISI450_2.TrialDetails.Driftspan]);
       ISI450_DSPAN_SEM = mean([ISI450_0.TrialDetails.Driftspan_SEM, ISI450_1.TrialDetails.Driftspan_SEM, ISI450_2.TrialDetails.Driftspan_SEM]);
       ISI450_DSPEED = mean([ISI450_0.TrialDetails.Driftspeed, ISI450_1.TrialDetails.Driftspeed, ISI450_2.TrialDetails.Driftspeed]);
       ISI450_DSPEED_SEM = mean([ISI450_0.TrialDetails.Driftspeed_SEM, ISI450_1.TrialDetails.Driftspeed_SEM, ISI450_2.TrialDetails.Driftspeed_SEM]);
       ISI750_DSPAN = mean([ISI750_0.TrialDetails.Driftspan, ISI750_1.TrialDetails.Driftspan, ISI750_2.TrialDetails.Driftspan]);
       ISI750_DSPAN_SEM = mean([ISI750_0.TrialDetails.Driftspan_SEM, ISI750_1.TrialDetails.Driftspan_SEM, ISI750_2.TrialDetails.Driftspan_SEM]);
       ISI750_DSPEED = mean([ISI750_0.TrialDetails.Driftspeed, ISI750_1.TrialDetails.Driftspeed, ISI750_2.TrialDetails.Driftspeed]);
       ISI750_DSPEED_SEM = mean([ISI750_0.TrialDetails.Driftspeed_SEM, ISI750_1.TrialDetails.Driftspeed_SEM, ISI750_2.TrialDetails.Driftspeed_SEM]);
       xarray = [5;60;450;750];
       yarray = [ISI5_DSPAN; ISI60_DSPAN; ISI450_DSPAN;ISI750_DSPAN];
       error_array = [ISI5_DSPAN_SEM; ISI60_DSPAN_SEM;  ISI450_DSPAN_SEM;ISI750_DSPAN_SEM];
       yarray2 = [ISI5_DSPEED; ISI60_DSPEED; ISI450_DSPEED; ISI750_DSPEED];
       error_array2 = [ISI5_DSPEED_SEM; ISI60_DSPEED_SEM; ISI450_DSPEED_SEM;ISI750_DSPEED_SEM];
elseif strcmp(control, 'fovea')
    if strcmp(subject, 'Giorgio')
       ISI10_DSPAN = mean([ISI10_0.TrialDetails.Driftspan, ISI10_1.TrialDetails.Driftspan, ISI10_2.TrialDetails.Driftspan]);
       ISI10_DSPAN_SEM = mean([ISI10_0.TrialDetails.Driftspan_SEM, ISI10_1.TrialDetails.Driftspan_SEM, ISI10_2.TrialDetails.Driftspan_SEM]);
       ISI10_DSPEED = mean([ISI10_0.TrialDetails.Driftspeed, ISI10_1.TrialDetails.Driftspeed, ISI10_2.TrialDetails.Driftspeed]);
       ISI10_DSPEED_SEM = mean([ISI10_0.TrialDetails.Driftspeed_SEM, ISI10_1.TrialDetails.Driftspeed_SEM, ISI10_2.TrialDetails.Driftspeed_SEM]);
       ISI60_DSPAN = mean([ISI60_0.TrialDetails.Driftspan, ISI60_1.TrialDetails.Driftspan, ISI60_2.TrialDetails.Driftspan]);
       ISI60_DSPAN_SEM = mean([ISI60_0.TrialDetails.Driftspan_SEM, ISI60_1.TrialDetails.Driftspan_SEM, ISI60_2.TrialDetails.Driftspan_SEM]);
       ISI60_DSPEED = mean([ISI60_0.TrialDetails.Driftspeed, ISI60_1.TrialDetails.Driftspeed, ISI60_2.TrialDetails.Driftspeed]);
       ISI60_DSPEED_SEM = mean([ISI60_0.TrialDetails.Driftspeed_SEM, ISI60_1.TrialDetails.Driftspeed_SEM, ISI60_2.TrialDetails.Driftspeed_SEM]);
       ISI150_DSPAN = mean([ISI150_0.TrialDetails.Driftspan, ISI150_1.TrialDetails.Driftspan, ISI150_2.TrialDetails.Driftspan]);
       ISI150_DSPAN_SEM = mean([ISI150_0.TrialDetails.Driftspan_SEM, ISI150_1.TrialDetails.Driftspan_SEM, ISI150_2.TrialDetails.Driftspan_SEM]);
       ISI150_DSPEED = mean([ISI150_0.TrialDetails.Driftspeed, ISI150_1.TrialDetails.Driftspeed, ISI150_2.TrialDetails.Driftspeed]);
       ISI150_DSPEED_SEM = mean([ISI150_0.TrialDetails.Driftspeed_SEM, ISI150_1.TrialDetails.Driftspeed_SEM, ISI150_2.TrialDetails.Driftspeed_SEM]);
       ISI200_DSPAN = mean([ISI200_0.TrialDetails.Driftspan, ISI200_1.TrialDetails.Driftspan, ISI200_2.TrialDetails.Driftspan]);
       ISI200_DSPAN_SEM = mean([ISI200_0.TrialDetails.Driftspan_SEM, ISI200_1.TrialDetails.Driftspan_SEM, ISI200_2.TrialDetails.Driftspan_SEM]);
       ISI200_DSPEED = mean([ISI200_0.TrialDetails.Driftspeed, ISI200_1.TrialDetails.Driftspeed, ISI200_2.TrialDetails.Driftspeed]);
       ISI200_DSPEED_SEM = mean([ISI200_0.TrialDetails.Driftspeed_SEM, ISI200_1.TrialDetails.Driftspeed_SEM, ISI200_2.TrialDetails.Driftspeed_SEM]);
       ISI450_DSPAN = mean([ISI450_0.TrialDetails.Driftspan, ISI450_1.TrialDetails.Driftspan, ISI450_2.TrialDetails.Driftspan]);
       ISI450_DSPAN_SEM = mean([ISI450_0.TrialDetails.Driftspan_SEM, ISI450_1.TrialDetails.Driftspan_SEM, ISI450_2.TrialDetails.Driftspan_SEM]);
       ISI450_DSPEED = mean([ISI450_0.TrialDetails.Driftspeed, ISI450_1.TrialDetails.Driftspeed, ISI450_2.TrialDetails.Driftspeed]);
       ISI450_DSPEED_SEM = mean([ISI450_0.TrialDetails.Driftspeed_SEM, ISI450_1.TrialDetails.Driftspeed_SEM, ISI450_2.TrialDetails.Driftspeed_SEM]);
       xarray = [10;60;450];
       yarray = [ISI10_DSPAN; ISI60_DSPAN; ISI450_DSPAN];
       error_array = [ISI10_DSPAN_SEM; ISI60_DSPAN_SEM;  ISI450_DSPAN_SEM];
       yarray2 = [ISI10_DSPEED; ISI60_DSPEED; ISI450_DSPEED];
       error_array2 = [ISI10_DSPEED_SEM; ISI60_DSPEED_SEM; ISI450_DSPEED_SEM];
    elseif strcmp(subject, 'Adriana')
       ISI5_DSPAN = mean([ISI5_0.TrialDetails.Driftspan, ISI5_1.TrialDetails.Driftspan, ISI5_2.TrialDetails.Driftspan]);
       ISI5_DSPAN_SEM = mean([ISI5_0.TrialDetails.Driftspan_SEM, ISI5_1.TrialDetails.Driftspan_SEM, ISI5_2.TrialDetails.Driftspan_SEM]);
       ISI5_DSPEED = mean([ISI5_0.TrialDetails.Driftspeed, ISI5_1.TrialDetails.Driftspeed, ISI5_2.TrialDetails.Driftspeed]);
       ISI5_DSPEED_SEM = mean([ISI5_0.TrialDetails.Driftspeed_SEM, ISI5_1.TrialDetails.Driftspeed_SEM, ISI5_2.TrialDetails.Driftspeed_SEM]); 
       ISI10_DSPAN = mean([ISI10_0.TrialDetails.Driftspan, ISI10_1.TrialDetails.Driftspan, ISI10_2.TrialDetails.Driftspan]);
       ISI10_DSPAN_SEM = mean([ISI10_0.TrialDetails.Driftspan_SEM, ISI10_1.TrialDetails.Driftspan_SEM, ISI10_2.TrialDetails.Driftspan_SEM]);
       ISI10_DSPEED = mean([ISI10_0.TrialDetails.Driftspeed, ISI10_1.TrialDetails.Driftspeed, ISI10_2.TrialDetails.Driftspeed]);
       ISI10_DSPEED_SEM = mean([ISI10_0.TrialDetails.Driftspeed_SEM, ISI10_1.TrialDetails.Driftspeed_SEM, ISI10_2.TrialDetails.Driftspeed_SEM]);
       ISI450_DSPAN = mean([ISI450_0.TrialDetails.Driftspan, ISI450_1.TrialDetails.Driftspan, ISI450_2.TrialDetails.Driftspan]);
       ISI450_DSPAN_SEM = mean([ISI450_0.TrialDetails.Driftspan_SEM, ISI450_1.TrialDetails.Driftspan_SEM, ISI450_2.TrialDetails.Driftspan_SEM]);
       ISI450_DSPEED = mean([ISI450_0.TrialDetails.Driftspeed, ISI450_1.TrialDetails.Driftspeed, ISI450_2.TrialDetails.Driftspeed]);
       ISI450_DSPEED_SEM = mean([ISI450_0.TrialDetails.Driftspeed_SEM, ISI450_1.TrialDetails.Driftspeed_SEM, ISI450_2.TrialDetails.Driftspeed_SEM]);
       ISI600_DSPAN = mean([ISI600_0.TrialDetails.Driftspan, ISI600_1.TrialDetails.Driftspan, ISI600_2.TrialDetails.Driftspan]);
       ISI600_DSPAN_SEM = mean([ISI600_0.TrialDetails.Driftspan_SEM, ISI600_1.TrialDetails.Driftspan_SEM, ISI600_2.TrialDetails.Driftspan_SEM]);
       ISI600_DSPEED = mean([ISI600_0.TrialDetails.Driftspeed, ISI600_1.TrialDetails.Driftspeed, ISI600_2.TrialDetails.Driftspeed]);
       ISI600_DSPEED_SEM = mean([ISI600_0.TrialDetails.Driftspeed_SEM, ISI600_1.TrialDetails.Driftspeed_SEM, ISI600_2.TrialDetails.Driftspeed_SEM]);
       ISI750_DSPAN = mean([ISI750_0.TrialDetails.Driftspan, ISI750_1.TrialDetails.Driftspan, ISI750_2.TrialDetails.Driftspan]);
       ISI750_DSPAN_SEM = mean([ISI750_0.TrialDetails.Driftspan_SEM, ISI750_1.TrialDetails.Driftspan_SEM, ISI750_2.TrialDetails.Driftspan_SEM]);
       ISI750_DSPEED = mean([ISI750_0.TrialDetails.Driftspeed, ISI750_1.TrialDetails.Driftspeed, ISI750_2.TrialDetails.Driftspeed]);
       ISI750_DSPEED_SEM = mean([ISI750_0.TrialDetails.Driftspeed_SEM, ISI750_1.TrialDetails.Driftspeed_SEM, ISI750_2.TrialDetails.Driftspeed_SEM]); 
       xarray = [5;10;450;600;750];
       yarray = [ISI5_DSPAN; ISI10_DSPAN; ISI450_DSPAN; ISI600_DSPAN; ISI750_DSPAN];
       error_array = [ISI5_DSPAN_SEM; ISI10_DSPAN_SEM; ISI450_DSPAN_SEM; ISI600_DSPAN_SEM; ISI750_DSPAN_SEM];
       yarray2 = [ISI5_DSPEED; ISI10_DSPEED; ISI450_DSPEED; ISI600_DSPEED; ISI750_DSPEED];
       error_array2 = [ISI5_DSPEED_SEM; ISI10_DSPEED_SEM; ISI450_DSPEED_SEM; ISI600_DSPEED_SEM; ISI750_DSPEED_SEM];      
    elseif strcmp(subject, 'Anna')
       ISI5_DSPAN = mean([ISI5_0.TrialDetails.Driftspan, ISI5_1.TrialDetails.Driftspan, ISI5_2.TrialDetails.Driftspan]);
       ISI5_DSPAN_SEM = mean([ISI5_0.TrialDetails.Driftspan_SEM, ISI5_1.TrialDetails.Driftspan_SEM, ISI5_2.TrialDetails.Driftspan_SEM]);
       ISI5_DSPEED = mean([ISI5_0.TrialDetails.Driftspeed, ISI5_1.TrialDetails.Driftspeed, ISI5_2.TrialDetails.Driftspeed]);
       ISI5_DSPEED_SEM = mean([ISI5_0.TrialDetails.Driftspeed_SEM, ISI5_1.TrialDetails.Driftspeed_SEM, ISI5_2.TrialDetails.Driftspeed_SEM]); 
       ISI10_DSPAN = mean([ISI10_0.TrialDetails.Driftspan, ISI10_1.TrialDetails.Driftspan, ISI10_2.TrialDetails.Driftspan]);
       ISI10_DSPAN_SEM = mean([ISI10_0.TrialDetails.Driftspan_SEM, ISI10_1.TrialDetails.Driftspan_SEM, ISI10_2.TrialDetails.Driftspan_SEM]);
       ISI10_DSPEED = mean([ISI10_0.TrialDetails.Driftspeed, ISI10_1.TrialDetails.Driftspeed, ISI10_2.TrialDetails.Driftspeed]);
       ISI10_DSPEED_SEM = mean([ISI10_0.TrialDetails.Driftspeed_SEM, ISI10_1.TrialDetails.Driftspeed_SEM, ISI10_2.TrialDetails.Driftspeed_SEM]);
       ISI60_DSPAN = mean([ISI60_0.TrialDetails.Driftspan, ISI60_1.TrialDetails.Driftspan, ISI60_2.TrialDetails.Driftspan]);
       ISI60_DSPAN_SEM = mean([ISI60_0.TrialDetails.Driftspan_SEM, ISI60_1.TrialDetails.Driftspan_SEM, ISI60_2.TrialDetails.Driftspan_SEM]);
       ISI60_DSPEED = mean([ISI60_0.TrialDetails.Driftspeed, ISI60_1.TrialDetails.Driftspeed, ISI60_2.TrialDetails.Driftspeed]);
       ISI60_DSPEED_SEM = mean([ISI60_0.TrialDetails.Driftspeed_SEM, ISI60_1.TrialDetails.Driftspeed_SEM, ISI60_2.TrialDetails.Driftspeed_SEM]);
       ISI150_DSPAN = mean([ISI150_0.TrialDetails.Driftspan, ISI150_1.TrialDetails.Driftspan, ISI150_2.TrialDetails.Driftspan]);
       ISI150_DSPAN_SEM = mean([ISI150_0.TrialDetails.Driftspan_SEM, ISI150_1.TrialDetails.Driftspan_SEM, ISI150_2.TrialDetails.Driftspan_SEM]);
       ISI150_DSPEED = mean([ISI150_0.TrialDetails.Driftspeed, ISI150_1.TrialDetails.Driftspeed, ISI150_2.TrialDetails.Driftspeed]);
       ISI150_DSPEED_SEM = mean([ISI150_0.TrialDetails.Driftspeed_SEM, ISI150_1.TrialDetails.Driftspeed_SEM, ISI150_2.TrialDetails.Driftspeed_SEM]);
       ISI200_DSPAN = mean([ISI200_0.TrialDetails.Driftspan, ISI200_1.TrialDetails.Driftspan, ISI200_2.TrialDetails.Driftspan]);
       ISI200_DSPAN_SEM = mean([ISI200_0.TrialDetails.Driftspan_SEM, ISI200_1.TrialDetails.Driftspan_SEM, ISI200_2.TrialDetails.Driftspan_SEM]);
       ISI200_DSPEED = mean([ISI200_0.TrialDetails.Driftspeed, ISI200_1.TrialDetails.Driftspeed, ISI200_2.TrialDetails.Driftspeed]);
       ISI200_DSPEED_SEM = mean([ISI200_0.TrialDetails.Driftspeed_SEM, ISI200_1.TrialDetails.Driftspeed_SEM, ISI200_2.TrialDetails.Driftspeed_SEM]);
       ISI450_DSPAN = mean([ISI450_0.TrialDetails.Driftspan, ISI450_1.TrialDetails.Driftspan, ISI450_2.TrialDetails.Driftspan]);
       ISI450_DSPAN_SEM = mean([ISI450_0.TrialDetails.Driftspan_SEM, ISI450_1.TrialDetails.Driftspan_SEM, ISI450_2.TrialDetails.Driftspan_SEM]);
       ISI450_DSPEED = mean([ISI450_0.TrialDetails.Driftspeed, ISI450_1.TrialDetails.Driftspeed, ISI450_2.TrialDetails.Driftspeed]);
       ISI450_DSPEED_SEM = mean([ISI450_0.TrialDetails.Driftspeed_SEM, ISI450_1.TrialDetails.Driftspeed_SEM, ISI450_2.TrialDetails.Driftspeed_SEM]);
       xarray = [5;10;60;450];
       yarray = [ISI5_DSPAN; ISI10_DSPAN; ISI60_DSPAN; ISI450_DSPAN];
       error_array = [ISI5_DSPAN_SEM; ISI10_DSPAN_SEM; ISI60_DSPAN_SEM;  ISI450_DSPAN_SEM];
       yarray2 = [ISI5_DSPEED; ISI10_DSPEED; ISI60_DSPEED; ISI450_DSPEED];
       error_array2 = [ISI5_DSPEED_SEM; ISI10_DSPEED_SEM; ISI60_DSPEED_SEM; ISI450_DSPEED_SEM];
    elseif strcmp(subject, 'Karina')
       ISI5_DSPAN = mean([ISI5_0.TrialDetails.Driftspan, ISI5_1.TrialDetails.Driftspan, ISI5_2.TrialDetails.Driftspan]);
       ISI5_DSPAN_SEM = mean([ISI5_0.TrialDetails.Driftspan_SEM, ISI5_1.TrialDetails.Driftspan_SEM, ISI5_2.TrialDetails.Driftspan_SEM]);
       ISI5_DSPEED = mean([ISI5_0.TrialDetails.Driftspeed, ISI5_1.TrialDetails.Driftspeed, ISI5_2.TrialDetails.Driftspeed]);
       ISI5_DSPEED_SEM = mean([ISI5_0.TrialDetails.Driftspeed_SEM, ISI5_1.TrialDetails.Driftspeed_SEM, ISI5_2.TrialDetails.Driftspeed_SEM]); 
       ISI60_DSPAN = mean([ISI60_0.TrialDetails.Driftspan]);
       ISI60_DSPAN_SEM = mean([ISI60_0.TrialDetails.Driftspan_SEM]);
       ISI60_DSPEED = mean([ISI60_0.TrialDetails.Driftspeed]);
       ISI60_DSPEED_SEM = mean([ISI60_0.TrialDetails.Driftspeed_SEM]);
       ISI450_DSPAN = mean([ISI450_0.TrialDetails.Driftspan, ISI450_1.TrialDetails.Driftspan, ISI450_2.TrialDetails.Driftspan]);
       ISI450_DSPAN_SEM = mean([ISI450_0.TrialDetails.Driftspan_SEM, ISI450_1.TrialDetails.Driftspan_SEM, ISI450_2.TrialDetails.Driftspan_SEM]);
       ISI450_DSPEED = mean([ISI450_0.TrialDetails.Driftspeed, ISI450_1.TrialDetails.Driftspeed, ISI450_2.TrialDetails.Driftspeed]);
       ISI450_DSPEED_SEM = mean([ISI450_0.TrialDetails.Driftspeed_SEM, ISI450_1.TrialDetails.Driftspeed_SEM, ISI450_2.TrialDetails.Driftspeed_SEM]);
       xarray = [5;60;450];
       yarray = [ISI5_DSPAN; ISI60_DSPAN; ISI450_DSPAN];
       error_array = [ISI5_DSPAN_SEM; ISI60_DSPAN_SEM;  ISI450_DSPAN_SEM];
       yarray2 = [ISI5_DSPEED; ISI60_DSPEED; ISI450_DSPEED];
       error_array2 = [ISI5_DSPEED_SEM; ISI60_DSPEED_SEM; ISI450_DSPEED_SEM];
    end
end

%%
figure()
line(xarray,yarray)
hold on
errorbar(xarray,yarray,error_array,'k','LineWidth',3);
ylabel('arcmin');
xlabel('ISI Level');
%legend('Valid-Invalid','Location','southeast','Orientation','horizontal');
set(gca,'fontsize',20)
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
xticks(xarray)
%xlim([-10 775])
t = strcat(subject,'_DriftSpan', control);
title(strcat(subject,' Drift Span: ',control));
saveas(gca,sprintf(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', subject,'/Images/', control, '/', 'General/',t)));
saveas(gca,sprintf(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', subject,'/Images/', control, '/', 'General/',t,'.png')));


figure()
line(xarray,yarray2)
hold on
errorbar(xarray,yarray2,error_array2,'k','LineWidth',3);
ylabel('arcmin');
xlabel('ISI Level');
%legend('Valid-Invalid','Location','southeast','Orientation','horizontal');
set(gca,'fontsize',20)
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
xticks(xarray)
%xlim([-10 775])
t = strcat(subject,'_DriftSpeed_',control);
title(strcat(subject,' Drift Speed: ',control));
saveas(gca,sprintf(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', subject,'/Images/', control, '/', 'General/',t)));
saveas(gca,sprintf(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', subject,'/Images/', control, '/', 'General/',t,'.png')));

close all