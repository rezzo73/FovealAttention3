clc;
clear all;

%TO DO:
%make it run drift only and then msdrift automatically (right now must manually change in 307-312)

% subject00 = ISI5 & subject0 = ISI10 & subject1 = ISI 60 and subject2 = ISI 150 ; 
%subject3 = ISI 200; subject4 = ISI450; subject5 = 600; subject6 =750;
%subject7 = 1000;
%Natalya_old is old flash (whole box) at ISI 60
%00, 0, 1, 4, 5, 6, 7 - fovea
%00, 1, 4 , 6 - periphery
subject = 'Anna00'; 
Fovea=0; %change for fovea vs. periphery (for loading & saving)

dist_thresh = 30;
index = isletter(subject);
index2 = ~isletter(subject);
name_index = subject(index);
number_index = subject(index2);

smoothing = 41;
cutseg = floor(smoothing/2);
subtrialcount = 0; 
TrialType = 0; %initializing (loops through all triltypes)

%setting pathways
if Fovea==1
    if strcmp(subject,'Natalya_old') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Natalya/Natalya120.mat')
    elseif strcmp(subject,'Natalya1') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Natalya_ISI60_135.mat')
        ISI = 60;
    elseif strcmp(subject,'Natalya2') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Natalya_ISI150_135.mat')
        ISI = 150;
    elseif strcmp(subject,'Rania1') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Rania_ISI60.mat')
        ISI = 60;
    elseif strcmp(subject,'Rania2') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Rania_ISI150.mat')
        ISI = 150;
    elseif strcmp(subject,'Giorgio0') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Giorgio_ISI10_130.mat')
        ISI = 10;
    elseif strcmp(subject,'Giorgio1') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Giorgio_ISI60_130.mat')
        ISI = 60;
    elseif strcmp(subject,'Giorgio2') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Giorgio_ISI150_130.mat')
        ISI = 150;
    elseif strcmp(subject,'Giorgio3') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Giorgio_ISI200_130.mat')
        ISI = 200;
    elseif strcmp(subject,'Giorgio4') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Giorgio_ISI450_130.mat')
        ISI = 450;
    elseif strcmp(subject,'Anna00') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Anna_ISI5_110.mat')
        ISI = 5;
    elseif strcmp(subject,'Anna0') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Anna_ISI10_110.mat')
        ISI = 10;
    elseif strcmp(subject,'Anna1') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Anna_ISI60_110.mat')
        ISI = 60;
        vt{1,321}=[];
        vt{1,600}=[];
    elseif strcmp(subject,'Anna2') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Anna_ISI150_110.mat')
        ISI = 150;
    elseif strcmp(subject,'Anna3') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Anna_ISI200_110.mat')
        ISI = 200;
        vt{1,89}=[];
    elseif strcmp(subject,'Anna4') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Anna_ISI450_110.mat')
        ISI = 450;
    elseif strcmp(subject,'Adriana00') %ISI 5
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Adriana_ISI5_131.mat')
        ISI = 5;
    elseif strcmp(subject,'Adriana0') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Adriana_ISI10_131.mat')
        ISI = 10;
    elseif strcmp(subject,'Adriana1') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Adriana_ISI60_131.mat')
        ISI = 60;
    elseif strcmp(subject,'Adriana4') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Adriana_ISI450_131.mat')
        ISI = 450;
    elseif strcmp(subject,'Adriana5') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Adriana_ISI600_131.mat')
        ISI = 600;
    elseif strcmp(subject,'Adriana6') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Adriana_ISI750_131.mat')
        ISI = 750;
    elseif strcmp(subject,'Adriana7') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Adriana_ISI1000_131.mat')
        ISI = 1000;
    elseif strcmp(subject,'Karina00') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Karina_ISI5_108.mat')
        ISI = 5; 
    elseif strcmp(subject,'Karina1') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Karina_ISI60_108.mat')
        ISI = 60;  
    elseif strcmp(subject,'Karina4') 
        load('/Volumes/APLAB/BACKUPS/Ezzo_Backup/FovealAttention3/ProcessedData/Karina_ISI450_108.mat')
        ISI = 450;  
    end
elseif Fovea==0
    if strcmp(subject,'Adriana00') 
        load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/ProcessedData_Periphery/Adriana_ISI5_120.mat')
        ISI = 5;  
    elseif strcmp(subject,'Adriana1') 
        load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/ProcessedData_Periphery/Adriana_ISI60_120.mat')
        ISI = 60;  
    elseif strcmp(subject,'Adriana4') 
        load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/ProcessedData_Periphery/Adriana_ISI450_120.mat')
        ISI = 450; 
    elseif strcmp(subject,'Adriana6') 
        load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/ProcessedData_Periphery/Adriana_ISI750_120.mat')
        ISI = 750;         
    elseif strcmp(subject,'Karina00') 
        load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/ProcessedData_Periphery/Karina_ISI5_103.mat')
        ISI = 5; 
    elseif strcmp(subject,'Karina1') 
        load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/ProcessedData_Periphery/Karina_ISI60_103.mat')
        ISI = 60; 
    elseif strcmp(subject,'Karina4') 
        load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/ProcessedData_Periphery/Karina_ISI450_103.mat')
        ISI = 450; 
    elseif strcmp(subject,'Karina6') 
        load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/ProcessedData_Periphery/Karina_ISI750_103.mat')
        ISI = 750; 
    elseif strcmp(subject,'Anna00') 
        load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/ProcessedData_Periphery/Anna_ISI5_90.mat')
        ISI = 5; 
        vt{1,275}=[];
        vt{1,777} =[]; %contains artifact
    elseif strcmp(subject,'Anna1') 
        load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/ProcessedData_Periphery/Anna_ISI60_90.mat')
        ISI = 60; 
        vt{1,951}=[];
        vt{1,241}=[];
    elseif strcmp(subject,'Anna4') 
        load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/ProcessedData_Periphery/Anna_ISI450_90.mat')
        ISI = 450; 
        vt{1,309}=[];
        vt{1,980}=[];
        vt{1,786}=[];
        vt{1,823}=[];
        vt{1,939}=[];
    elseif strcmp(subject,'Anna6') 
        load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/ProcessedData_Periphery/Anna_ISI750_90.mat')
        ISI = 750; 
        vt{1,1966}=[];
        vt{1,2013}=[];
    end
end

empties = find(cellfun(@isempty,vt));
vt(empties) = [];

%initializing for count (to ensure output is correct)
count = struct('NT', zeros(size(vt)),...
    'NA', zeros(size(vt)),...
    'MS', zeros(size(vt)),...
    'DriftOnly', zeros(size(vt)),...
    'TotalTrialTypeCount', zeros(size(vt)),...
    'Correct', zeros(size(vt)),... 
    'Incorrect', zeros(size(vt)),... 
    'Correct_DriftOnly', zeros(size(vt)),...
    'Correct_MS', zeros(size(vt)),...    
    'Incorrect_DriftOnly', zeros(size(vt)),...
    'Incorrect_MS', zeros(size(vt)),...
    'S', zeros(size(vt)), ...
    'BadRT', zeros(size(vt)), ...
    'BigOffset', zeros(size(vt)), ...
    'msAmp', zeros(size(vt)), ...
    'msrate', zeros(size(vt)));

for ii = 1:length(vt)
    
   %know max trial length (for later) 
   trial_length(ii) = length(vt{ii}.x.position); 
    
   if strcmp(subject,'Natalya0') 
   vt{ii}.TimePesponseCueOFF = 1100;
   end
    
    %adding variables to vt
    vt{ii}.targetlocations(1) = vt{ii}.Target1_Orientation;
    vt{ii}.targetlocations(2) = vt{ii}.Target2_Orientation;
    vt{ii}.targetlocations(3) = vt{ii}.Target3_Orientation;
    vt{ii}.targetlocations(4) = vt{ii}.Target4_Orientation;
    
    %initializing arrays
    targetorientation(ii) = NaN;
    responsetime(ii) = NaN;
    responsevalue(ii) = NaN;
    
    %initializing: , LABELING NT, S, M, DRIFT
    vt{ii}.NA = 0; 
    vt{ii}.NT = 0;
    vt{ii}.S = 0;
    vt{ii}.MS = 0;
    vt{ii}.BadRT = 0;
    vt{ii}.BigOffset = 0;
    vt{ii}.DriftOnly = 0;
    vt{ii}.BadRT;

    
    %including response value (for d prime)
    if (vt{ii}.Correct == 1)
      vt{ii}.responsevalue = vt{ii}.targetlocations(vt{ii}.ResponseCueLocation + 1);
    else
      vt{ii}.responsevalue = abs(vt{ii}.targetlocations(vt{ii}.ResponseCueLocation + 1)-1);
    end

    %In c++ code, every trial was assigned a cue location, including 
    %neutral trials. This corrects for this.)
   if vt{ii}.TrialType == 2
       vt{ii}.PreTargetCueLocation = [];
   end
    
    
   %FILTERING TRIALS: FIXING OVERSHOOT

   %removing overshoot
   vt{ii} = osRemover(vt{ii}, vt{ii}.microsaccades);
    


%%%%%%%%%%%%%%%% FILTER OUT BAD TRIALS %%%%%%%%%%%%%%%%%%%%%%%%%%%

   
    %need actual response duration
    vt{ii}.Target2Response = round((vt{ii}.ResponseTime) - (vt{ii}.TimePesponseCueOFF+100)); 
    %100 ms is interval in the c++ code
    
    %find time intervals for each trial (added the response cue into this
    %interval
    hook = 30;
    t1 = round(vt{ii}.TimeCueON - hook);
    %t2 = round(vt{ii}.TimeTargetOFF + vt{ii}.TimePesponseCueOFF);
    t2 = round(vt{ii}.TimePesponseCueOFF + hook);
    B = round(vt{ii}.TimeTargetON);
    
    %use intervals to filter out bad trials during cue-target presentation
       if  isIntersectedIn(t1, t2-t1, vt{ii}.invalid) || ...
           isIntersectedIn(t1, t2-t1, vt{ii}.blinks) || ...
           isIntersectedIn(t1, t2-t1, vt{ii}.notracks) || ...
           vt{ii}.Response == 2 || ... %no answer
           ii == 2013 % Need for Anna (remove later)
           count.NT(ii) = count.NT(ii) + 1;
           vt{ii}.NT = 1;
           continue; % disregard these trials  
       end

       % Gaze too far from center
       if sum(abs(vt{ii}.x.position(t1:(t2-t1)) + vt{ii}.xoffset) > dist_thresh) > 0 || ...
          sum(abs(vt{ii}.y.position(t1:(t2-t1)) + vt{ii}.yoffset) > dist_thresh) > 0
          count.BigOffset(ii) = count.BigOffset(ii) + 1;
          vt{ii}.BigOffset = 1;
          continue;
       end
        % TOO SHORT (< 200) OR TOO LONG (> 1000) RESPONSE TIMES
        if (vt{ii}.ResponseTime < 200 || vt{ii}.ResponseTime < 1000)
           count.BadRT(ii) = count.BadRT(ii) + 1;
           vt{ii}.BadRT = 1;
           continue;
        end  
       
       if ~isIntersectedIn(t1, t2-t1, vt{ii}.invalid) && ...
           ~isIntersectedIn(t1, t2-t1, vt{ii}.blinks) && ...
           ~isIntersectedIn(t1, t2-t1, vt{ii}.notracks)
            if isIntersectedIn(t1, t2-t1, vt{ii}.saccades),...
            count.S(ii) = count.S(ii) + 1;
            vt{ii}.S = 1;
            continue; % disregard these trials  
            end
       end
 
       if ~isIntersectedIn(t1, t2-t1, vt{ii}.invalid) && ...
           ~isIntersectedIn(t1, t2-t1, vt{ii}.blinks) && ...
           ~isIntersectedIn(t1, t2-t1, vt{ii}.notracks) && ...
           ~isIntersectedIn(t1, t2-t1, vt{ii}.saccades)
                if isIntersectedIn(t1, t2-t1, vt{ii}.microsaccades),...
                count.MS(ii) = count.MS(ii) + 1;
                vt{ii}.MS = 1;
                continue; % disregard these trials  
                end
       end
     
       
        if ~isIntersectedIn(t1, t2-t1, vt{ii}.invalid) && ...
           ~isIntersectedIn(t1, t2-t1, vt{ii}.blinks) && ...
           ~isIntersectedIn(t1, t2-t1, vt{ii}.notracks) && ...
           ~isIntersectedIn(t1, t2-t1, vt{ii}.saccades) && ...
           ~isIntersectedIn(t1, t2-t1, vt{ii}.microsaccades),...
                count.DriftOnly(ii) = count.DriftOnly(ii) + 1;
                vt{ii}.DriftOnly = 1;
                continue; % disregard these trials  
        end
        
end

%%
%%ANALYSIS OF TRIAL TYPE:

TrialType=0;

for tt = 1:3   
    total = 0;
    add = 0;  
    
  for ii = 1:length(vt) 
    if (((ii == 69) || (ii == 70)) && (strcmp(subject, 'Karina00')))
    elseif (((ii == 69) || (ii == 70)) && (strcmp(subject, 'Karina1')))
    elseif (((ii == 127) || (ii == 129)) && (strcmp(subject, 'Adriana1')))
    elseif ((ii == 148) && (strcmp(subject, 'Adriana6')))
        continue
    else
    %find time intervals for each trial
    t1 = round(vt{ii}.TimeCueON - hook);
    t2 = round(vt{ii}.TimeTargetOFF + hook);
    %B = round(vt{ii}.TimeTargetON);
    %RT(ii) = round((vt{ii}.ResponseTime+100) - (vt{ii}.TimeTargetOFF + vt{ii}.TimePesponseCueOFF));
    
    %added: gaze between cue-target:: NEW NEW NEW
       mYX{ii} = NaN (length(vt{ii}.x.position(t1:t2)),1);
       mYX{ii} = NaN (length(vt{ii}.y.position(t1:t2)),2);
       mYX{ii}(:,1) = vt{ii}.x.position(t1:t2) + vt{ii}.xoffset;
       mYX{ii}(:,2) = vt{ii}.y.position(t1:t2) + vt{ii}.yoffset;
    
   %if vt{ii}.TrialType==TrialType
    %looking at drift and ms trials
    
    %overall msAmp (no filtered trials--no filter of time)
     count.msAmp(ii) = mean(vt{ii}.microsaccades.amplitude);
%     
    if ((vt{ii}.TrialType==TrialType) && ((vt{ii}.DriftOnly > 0)  || vt{ii}.MS > 0))
        Include = 'MsDrift';
         
      %only include specified trial type (e.g. neutral) in analysis
%       if ((vt{ii}.TrialType==TrialType) && (vt{ii}.DriftOnly > 0))
%          Include = 'Drift';
         

        count.TotalTrialTypeCount(ii) = count.TotalTrialTypeCount(ii) + 1;
        targetorientation(ii) = vt{ii}.targetlocations(vt{ii}.ResponseCueLocation + 1);
        %responsetime(ii) = vt{ii}.ResponseTime;
        responsetime(ii) = round((vt{ii}.ResponseTime) - (vt{ii}.TimePesponseCueOFF+100)); %new
        %RT(ii) = RT(ii);
        responsevalue(ii) = vt{ii}.responsevalue;
        total = total + 1;
        if vt{ii}.Correct
        add = add + 1;
        %for ii = 1:length(vt)
        end
     else
        targetorientation(ii) = NaN;
        %RT(ii) = NaN;
        responsetime(ii) = NaN;
        responsevalue(ii) = NaN;
     end
    end
  end
 
  
  
  
  
targetorientation(isnan(targetorientation)) = [];
%RT(isnan(RT)) = [];
responsetime(isnan(responsetime)) = [];
responsevalue(isnan(responsevalue)) = [];

%no longer need this.
%responsetime(:) = responsetime - (ISI + 1240); %fixes response time error.

%average response time:
%Ave_ResponseTime = mean(RT);
Ave_ResponseTime = mean(responsetime);
Ave_ResponseTime_SEM = std(responsetime)/sqrt(length(responsetime));

%NOT SURE ABOUT WHAT OUTPUT IS
%output: d prime, standard deviation, confidence interval, critical
[d std_d ci_d crit var_d] = CalculateDprime_2(responsevalue, targetorientation);

%fprintf('TRIAL TYPE %i\n',TrialType);
fprintf('No Answer - %i, No Track - %i, Saccades - %i, Microaccades - %i, Drift - %i, BigOffset - %i, BadRT - %i\n',...
        sum(count.NA), sum(count.NT), sum(count.S), sum(count.MS), sum(count.DriftOnly), sum(count.BigOffset), sum(count.BadRT));



%load in the data for all trial types

Trialtype = sprintf('TrialType%d',TrialType);

Name = strcat('Data ',number_index,Trialtype);

%initializing for count (to ensure output is correct)
Data = struct('Subject', name_index,...
    'Fovea', Fovea,...
    'Type', TrialType,...
    'NumberCorrect', add,...
    'NumberTotal', total,...
    'dprime', d,...
    'std_dprime', std_d,...
    'ci_dprime', ci_d,... 
    'crit_dprime', crit,...
    'var_dprime', var_d,...
    'AveResponseTime', Ave_ResponseTime, ...
    'AveResponseTime_SEM', Ave_ResponseTime_SEM, ...
    'ISI', ISI);

eval(strcat(Name, ' = Data'));

    if Fovea==1
        control='fovea';
    elseif Fovea==0
        control='periphery';
    end

    save(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', name_index,'/Data/', control, '/', Include, '/', Name,'.mat'), 'Data');   

    clear('Data');
    
    
TrialType = TrialType+1;

end

%Now save variables that are the same across trial types:

Name2 = sprintf('GeneralStats_ISI%d',ISI);

GeneralStats = struct('BigOffset', sum(count.BigOffset),...
    'BadRT', sum(count.BadRT),...
    'NoAnswer', sum(count.NA),...
    'NoTrack', sum(count.NT),...
    'Saccades', sum(count.S),...
    'Microaccades', sum(count.MS), ...
    'Drift', sum(count.DriftOnly));

eval(strcat(Name2, ' = GeneralStats'));

%save general stats
save(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', name_index,'/Data/', control,'/', Include, '/', Name2,'.mat'), 'GeneralStats');  

clear('GeneralStats')

%%
%%%%%%%%%%%%%%%%% MS DETAILS %%%%%%%%%%%%%%%%% 

%take approx trial length
%max_trial_length = floor(mean(trial_length)); %use mean b/c outliers exist
max_trial_length = 3000; %standardize

if strcmp(Include, 'MsDrift')
    TrialType = 0; %need to switch ADD LATER

    for gg = 1:3
        
    %create black matrix with white for ms (trials as rows; ms over time)
    ms_over_time = nan(length(vt), max_trial_length);    

        for ii = 1:length(vt)
          if ((ii == 777) && (strcmp(subject, 'Anna00'))) || ...
                  ((ii == 1966) && (strcmp(subject, 'Anna6'))) || ...
                  ((ii == 148) && (strcmp(subject, 'Adriana6')))
              continue
          else
            t1_save(ii) = nan;
            E_save(ii) = nan;
            vt{ii}.driftspeed = [];
            vt{ii}.driftspan = [];
            vt{ii}.trialdrift_xpositions = [];
            vt{ii}.trialdrift_ypositions = [];

          if ((vt{ii}.TrialType==TrialType) && ((vt{ii}.DriftOnly > 0)  || vt{ii}.MS > 0))

            ms_over_time(ii,:) = 0; 

            t1 = round(vt{ii}.TimeCueON - hook);
            t1_save(ii) = round(vt{ii}.TimeCueON);
            E = round(vt{ii}.TimeTargetOFF + hook);
            E_save(ii) = round(vt{ii}.TimeTargetOFF);

                 if vt{ii}.MS == 1 %breaking up the ms and drift within each trial

                    To_fix = t1:E;
                    Time_array = t1:E;
                    for pi = 1:(numel(vt{ii}.microsaccades.start))

                      M1 = round(vt{ii}.microsaccades.start(pi));
                      ME = round(vt{ii}.microsaccades.start(pi) + vt{ii}.microsaccades.duration(pi));

                      Ms_array{1,pi} = M1:ME; %all ms info in trial
                      %ms_over_time(ii,M1:ME) = 255; %plot all ms in matrix
                      ms_over_time(ii,M1:M1+10) = 255; %plot all ms in matrix
                      Acommon{pi} = intersect(Ms_array{1,pi},t1:E);
                      Amplitude_selected_ms{pi} = vt{ii}.microsaccades.amplitude(pi); %added Feb
                      Time_ms{pi} = Time_array(ismember(Time_array, Acommon{pi}));

                      To_Eliminate = [Ms_array{1,:}];
                      Drift_array = ismember(t1:E,To_Eliminate);
                      To_fix(Drift_array) = nan;
                      To_fix = To_fix';

                      i1 = all(~isnan(To_fix),2);
                      i2 = i1(:)';
                      idx = [strfind([~i2(1),i2],[0 1]); strfind([i2, ~i2(end)],[1 0])];
                      B = mat2cell(To_fix(i1,:),diff(idx)+1,size(To_fix,2));
                      B = B';

                              for ji = 1:numel(B)
                                if isempty(B{1,ji}) 
                                continue
                                else
                                  B{1,ji} = B{1,ji}';
                                  X_drift{ji} = vt{ii}.x.position(B{ji}) + vt{ii}.xoffset;
                                  Y_drift{ji} = vt{ii}.y.position(B{ji}) + vt{ii}.yoffset;

                                  vt{ii}.trialdrift_xpositions{ji} = X_drift{ji};
                                  vt{ii}.trialdrift_ypositions{ji} = Y_drift{ji};
                               end
                              end


                         if isempty(Time_ms{pi}) %ms occurs but not in interval
                             %vt{ii}.trialms_xpositions{1} = [];
                             %vt{ii}.trialms_ypositions{1} = [];
                             %%vt{ii}.trialms_amplitude{1} = []; %added Feb
                             continue
                         elseif ((min(Time_ms{pi}) <= t1) && (max(Time_ms{pi}) >= t1)) ... 
                            ||  ((min(Time_ms{pi}) >= t1) && (max(Time_ms{pi}) <= E)) ...
                            ||  ((min(Time_ms{pi}) <= E) && (max(Time_ms{pi}) <= E))
                              count.msrate(ii) = count.msrate(ii) + 1;

                              %not all MS trials will have this--only ones within
                              %range.
                              %%vt{ii}.trialms_amplitude{pi} = Amplitude_selected_ms{pi}; %added Feb
                              vt{ii}.trialms_xpositions{pi} = vt{ii}.x.position(Time_ms{pi}) + vt{ii}.xoffset;
                              vt{ii}.trialms_ypositions{pi} = vt{ii}.y.position(Time_ms{pi}) + vt{ii}.yoffset;
                         end
                    end 
                     Mean_Selected_amp(ii) = mean(Amplitude_selected_ms{pi});
                 else %non ms trials
                          %%vt{ii}.trialms_amplitude{1} = []; %added Feb     
                          vt{ii}.trialms_xpositions{1} = [];
                          vt{ii}.trialms_ypositions{1} = [];
                          vt{ii}.trialdrift_xpositions{1} = vt{ii}.x.position(t1:E) + vt{ii}.xoffset;
                          vt{ii}.trialdrift_ypositions{1} = vt{ii}.y.position(t1:E) + vt{ii}.yoffset;
                 end

                  %finding drift span & speed
                  x = vt{ii}.trialdrift_xpositions;
                  y = vt{ii}.trialdrift_ypositions;  

                for pp = 1:numel(vt{ii}.trialdrift_xpositions)
                %span
                subtrialcount = subtrialcount + 1;
                vt{ii}.driftspan(1,pp) = sqrt(max((x{1,pp} - mean(x{1,pp})).^2 + (y{1,pp} - mean(y{1,pp})).^2));
                fixtemp(subtrialcount) = struct('x', x{1,pp}, 'y', y{1,pp});


                    if length(x{1,pp}) <41 %needs to be at least a certain length
                        continue
                    else
                      %drift speed
                      xspeed{1,pp} = sgfilt(x{1,pp}, 3, smoothing, 1);
                      yspeed{1,pp} = sgfilt(y{1,pp}, 3, smoothing, 1);
                      vt{ii}.driftspeed(1,pp) = mean((sqrt(xspeed{1,pp}(cutseg:end-cutseg).^2 + yspeed{1,pp}(cutseg:end-cutseg).^2))*1000);
                    end
                end

                      DriftSpan(ii) = max(vt{ii}.driftspan);
                      DriftSpeed(ii) = mean(vt{ii}.driftspeed);


          end
          end
        end

        DriftSpan(DriftSpan == 0) = NaN;
        DriftSpan(isnan(DriftSpan)) = [];
        Driftspan = max(DriftSpan);
        Driftspan_SEM = std(DriftSpan)/sqrt(length(DriftSpan));
        DriftSpeed(DriftSpeed == 0) = NaN;
        DriftSpeed(isnan(DriftSpeed)) = [];
        Driftspeed = nanmean(DriftSpeed); 
        Driftspeed_SEM = std(DriftSpeed)/sqrt(length(DriftSpeed));

        %ms amp during interval (cue onset to target offset)
        Mean_Selected_amp(Mean_Selected_amp == 0) = NaN;
        Mean_Selected_amp(isnan(Mean_Selected_amp)) = [];
        MS_mean_amp = mean(Mean_Selected_amp, 'omitnan'); %only within selected interval
        %make sure this is correct
        MS_Amp_SEM = std(Mean_Selected_amp)/sqrt(length(Mean_Selected_amp));
        
        % need to fix this!
        % [~,~,DCoef_Dsq, ~,Dsq, SingleSegmentDsq] = CalculateDiffusionCoef(fixtemp);
        % Final_Diffusion_Coefficient = DCoef_Dsq * (1.46^2);

        %print ms details for trial type
        fprintf('For trial type: %i\n',...
                TrialType);
        fprintf('Driftspan: %i\n',...
                Driftspan); 
        fprintf('DriftspanError: %i\n',...
                Driftspan_SEM);             
        fprintf('Driftspeed: %i\n',...
                Driftspeed); 
        fprintf('DriftspeedError: %i\n',...
                Driftspeed_SEM); 
        fprintf('MSamplitude: %i\n',...
                 MS_mean_amp); 
        fprintf('MSamplitudeError: %i\n',...
                 MS_Amp_SEM); 
       
        Trialtype = sprintf('TrialType%d',TrialType);     
        %save all of this data
        Name3 = strcat(sprintf('MSDetails_ISI%d',ISI), Trialtype);

        TrialDetails = struct('TrialType', TrialType,...
    'Driftspan', Driftspan,...
    'Driftspan_SEM', Driftspan_SEM,...
    'Driftspeed', Driftspeed,...
    'Driftspeed_SEM', Driftspeed_SEM,...
    'MS_mean_amp', MS_mean_amp, ...
    'MS_Amp_SEM', MS_Amp_SEM);

eval(strcat(Name3, ' = TrialDetails'));

%save all 3 trial type MS details
%save(sprintf(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', name_index,'/Data/', control, '/MsDetails/', Name3,'.mat'))); 
save(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', name_index,'/Data/', control, ...
        '/MsDetails/', Name3, '.mat'), 'TrialDetails');

clear('TrialDetails');
    
    %%%           
             
                         
        %look at ms binary occurance in ms over trial
        ave_t1 = nanmean(t1_save);
        ave_E = nanmean(E_save);
        out = ms_over_time(all(~isnan(ms_over_time),2),:);
        out(:,ceil(ave_t1):ceil(ave_t1)+4) = 255; %should not change the values 
        out(:,ceil(ave_E):ceil(ave_E)+4) = 255;
        %imshow(out) %when ms happen over trial
        %imwrite(out, 'test.png');
        out = out(:,1:max_trial_length);
        figure()
        imagesc(out);
        title(sprintf('Trial Frequency of microsaccade per milisecond for ISI %d', ISI))
        ylabel('trial number')
        xlabel('milisecond')

        %save image
        saveas(gcf, sprintf(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', name_index,'/Images/', control, '/MsDetails/', sprintf('MsOverTime_ISI%d', ISI), sprintf('TrialType%d', TrialType), 'AllTrials.jpg')));

        %plot binary frequency of ms over trial time
%         figure()
%         out2 = ms_over_time(all(~isnan(ms_over_time),2),:);
%         out2(out2 == 255) = 1;
%         freq_graph = sum(out2);
%         plot(freq_graph(:,1:max_trial_length))
%         axis([0 3000 0 (max(sum(out2))+5)])
%         hold on
%         vline(ceil(ave_t1))
%         vline(ceil(ave_E))
%         title(sprintf('Cumulative Frequency of microsaccade per milisecond for ISI %d', ISI))
%         ylabel('amount of trials')
%         xlabel('milisecond')

    %save plot    
    saveas(gcf, sprintf(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', name_index,'/Images/', control, '/MsDetails/', ...
          sprintf('MsOverTime_ISI%d', ISI), sprintf('TrialType%d', TrialType),'SumTrials.jpg')));
    %save(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', name_index,'/Data/', control, ...
    %    '/MsDetails/', sprintf('freq_graph_ISI%d', ISI), Trialtype, '.mat'), 'freq_graph'); 

     TrialType = TrialType + 1;
    end
    
end



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%THE ANALYSIS BELOW IS ONLY IF RUNNING WITH MS %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%NOTE: ONLY FOR MS

if strcmp(Include, 'MsDrift')

    %initialize
    count0 = 0;
    total_cued_left = 0;
    total_cued_right = 0;
    total_no_cue = 0;
    ms_cue_left = 0;
    ms_cue_right = 0;
    %correct_no_cue = 0;

    %may be just of one trial type %this is best so far
    for ii = 1:length(vt) 
        if ((~isfield(vt{ii}, 'trialms_xpositions')) || (isempty(vt{ii}.trialms_xpositions{1}))) %check first block to see if just empty field
        %switched order so below happens for neutral, valid and invalid
        else
               temp_len = length(vt{ii}.trialms_xpositions);
               ms_array_temp = length(vt{ii}.trialms_xpositions{1});
               %get array of ms periods
               count0 = count0 + 1;
               ms_array_all(count0) = ms_array_temp;
               %
           if ((vt{ii}.MS || vt{ii}.DriftOnly) && (vt{ii}.TrialType<2))
               for qq = 1:length(temp_len)
                   if ((ii == 4771) && (strcmp(subject, 'Anna6'))) %trial not working
                       continue
                   end
                    if     (vt{ii}.PreTargetCueLocation == 0 || vt{ii}.PreTargetCueLocation == 2)
                            total_cued_left = total_cued_left+1;
                            End_MS_left_x(total_cued_left) = vt{ii}.trialms_xpositions{qq}(length(vt{ii}.trialms_xpositions{qq})-1);
                            End_MS_left_y(total_cued_left) = vt{ii}.trialms_ypositions{qq}(length(vt{ii}.trialms_ypositions{qq})-1);
                            %End_MS_left_x(total_cued_left) = mean(vt{ii}.trialms_xpositions{qq});
                            %End_MS_left_y(total_cued_left) = mean(vt{ii}.trialms_ypositions{qq});
                            if (End_MS_left_x(total_cued_left) < 0) %((vt{ii}.Correct == 1) && 
                                ms_cue_left = ms_cue_left + 1;
                            end
                    else (vt{ii}.PreTargetCueLocation == 1 || vt{ii}.PreTargetCueLocation == 3)
                            total_cued_right = total_cued_right+1;  
                            End_MS_right_x(total_cued_right) = vt{ii}.trialms_xpositions{qq}(length(vt{ii}.trialms_xpositions{qq})-1);
                            End_MS_right_y(total_cued_right) = vt{ii}.trialms_ypositions{qq}(length(vt{ii}.trialms_ypositions{qq})-1);
                            %End_MS_right_x(total_cued_right) = mean(vt{ii}.trialms_xpositions{qq});
                            %End_MS_right_y(total_cued_right) = mean(vt{ii}.trialms_ypositions{qq});
                            if (End_MS_right_x(total_cued_right) > 0) %((vt{ii}.Correct == 1) && 
                                ms_cue_right = ms_cue_right + 1;
                            end
                    end
               end
            elseif ((vt{ii}.MS || vt{ii}.DriftOnly) && (vt{ii}.TrialType==2)) % neutral trials
               temp_len = length(vt{ii}.trialms_xpositions);
               for qq = 1:length(temp_len)
                   total_no_cue = total_no_cue + 1;
                    End_MS_x(total_no_cue) = vt{ii}.trialms_xpositions{qq}(length(vt{ii}.trialms_xpositions{qq})-1);
                    End_MS_y(total_no_cue) = vt{ii}.trialms_ypositions{qq}(length(vt{ii}.trialms_ypositions{qq})-1);
%                     if (End_MS_right_x(total_cued_right) %(vt{ii}.Correct == 1)
%                         correct_no_cue = correct_no_cue + 1;
%                     end    
                   %End_MS_x(total_no_cue) = mean(vt{ii}.trialms_xpositions{qq});
                   %End_MS_y(total_no_cue) = mean(vt{ii}.trialms_ypositions{qq});  
               end
            end
         end %added
    end
    %end

MS_cue_left = ms_cue_left/total_cued_left; 
MS_cue_right = ms_cue_right/total_cued_right; 

%print landing pos in relation to pre-cue
fprintf('MS between cue-target by landing position in relation to cue\n')
fprintf('MS&cue_left: %i\n',...
                MS_cue_left);
fprintf('total_cued_left: %i\n',...
                total_cued_left); 
fprintf('MS&cue_right: %i\n',...
                 MS_cue_right); 
fprintf('total_cued_right: %i\n',...
                total_cued_right);
fprintf('total_no_cue: %i\n',...
                 total_no_cue);
             
%%%
     %save all of this data (valid,invalid combined, neutral separate)
     Name4 = sprintf('MSLanding2Cue_ISI%d',ISI);

        MSLanding2Cue = struct('MS_cue_left', MS_cue_left, ...
    'total_cued_left', total_cued_left, ...
    'MS_cue_right', MS_cue_right, ...
    'total_cued_right', total_cued_right, ...
    'total_no_cue', total_no_cue);


eval(strcat(Name4, ' = MSLanding2Cue'));

%save all 3 trial type MS details
save(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', name_index,'/Data/', control, '/MsDetails/', Name4,'.mat'), 'MSLanding2Cue');           

clear('MSLanding2Cue'); 
%%%             
             
         
%longest trial ms (for next section)
max_ms_within_trial = max(ms_array_all);
sum_ms_within_trials = total_no_cue + total_cued_right + total_cued_left;


%fixing format for 2dhistogram
if exist('End_MS_left_x')
    End_MS_left_x = End_MS_left_x';
    End_MS_left_y = End_MS_left_y';
    End_MS_left_final = [End_MS_left_x, End_MS_left_y];
else
    End_MS_left_final = [];
end

if exist('End_MS_right_x')
    End_MS_right_x = End_MS_right_x';
    End_MS_right_y = End_MS_right_y';
    End_MS_right_final = [End_MS_right_x, End_MS_right_y];
else
    End_MS_right_final = [];
end

if exist('End_MS_x')
    End_MS_x = End_MS_x';
    End_MS_y = End_MS_y';
    End_MS_final = [End_MS_x, End_MS_y];
else
    End_MS_final = [];
end


%%histogram of ms landing positions

if isempty(End_MS_left_final)
    End_MS_left_final = [10000,10000]; %pseudonumbers (to plot empty histogram)
end
figure()
  vXEdge = linspace(-50,50,25);
  vYEdge = linspace(-50,50,25);
  average_overlay = mean(End_MS_left_final); %%% ADDED
  mHist2d = hist2d(End_MS_left_final,vYEdge,vXEdge);
  %legend('Average landing pos')
  nXBins = length(vXEdge);
  nYBins = length(vYEdge);
  vXLabel = 0.5*(vXEdge(1:(nXBins-1))+vXEdge(2:nXBins));
  vYLabel = 0.5*(vYEdge(1:(nYBins-1))+vYEdge(2:nYBins));
  pcolor(vXLabel, vYLabel,mHist2d); colorbar
  grid off
  axis square
  shading interp
  hold on
  plot(average_overlay(1),average_overlay(2), 'r*'); %% ADDED
  title('CUE LEFT: Cue-target MS Landing Pos')
  
saveas(gcf, sprintf(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', name_index,'/Images/', control, '/MsDetails/', sprintf('MsLanding_ISI%d', ISI), '_CueLeft.jpg')));

if isempty(End_MS_right_final)
    End_MS_right_final = [10000,10000]; %pseudonumbers (to plot empty histogram)
end
 % next
 figure()
  vXEdge = linspace(-50,50,25);
  vYEdge = linspace(-50,50,25);
  average_overlay = mean(End_MS_right_final); %%% ADDED
  mHist2d = hist2d(End_MS_right_final,vYEdge,vXEdge);
  %legend('Average landing pos')
  nXBins = length(vXEdge);
  nYBins = length(vYEdge);
  vXLabel = 0.5*(vXEdge(1:(nXBins-1))+vXEdge(2:nXBins));
  vYLabel = 0.5*(vYEdge(1:(nYBins-1))+vYEdge(2:nYBins));
  pcolor(vXLabel, vYLabel,mHist2d); colorbar
  grid off
  axis square
  shading interp
  hold on
  plot(average_overlay(1),average_overlay(2), 'r*'); %% ADDED
  title('CUE RIGHT: Cue-target MS Landing Pos')
 
saveas(gcf, sprintf(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', name_index,'/Images/', control, '/MsDetails/', sprintf('MsLanding_ISI%d', ISI), '_CueRight.jpg')));  

if isempty(End_MS_final)
    End_MS_final = [10000,10000]; %pseudonumbers (to plot empty histogram)
end
% next
 figure()
  vXEdge = linspace(-50,50,25); %was 100
  vYEdge = linspace(-50,50,25);
  average_overlay = mean(End_MS_final); %%% ADDED
  mHist2d = hist2d(End_MS_final,vYEdge,vXEdge);
  %legend('Average landing pos')
  nXBins = length(vXEdge);
  nYBins = length(vYEdge);
  vXLabel = 0.5*(vXEdge(1:(nXBins-1))+vXEdge(2:nXBins));
  vYLabel = 0.5*(vYEdge(1:(nYBins-1))+vYEdge(2:nYBins));
  pcolor(vXLabel, vYLabel,mHist2d); colorbar
  grid off
  axis square
  shading interp
  hold on
  plot(average_overlay(1),average_overlay(2), 'r*'); %% ADDED
  title('NO CUE: Cue-target MS Landing Pos')
  
saveas(gcf, sprintf(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', name_index,'/Images/', control, '/MsDetails/', sprintf('MsLanding_ISI%d', ISI), '_Neutral.jpg')));
end

%%
%frequency of ms

if strcmp(Include, 'MsDrift')

%initialize matrix
ms_matrix_freq = nan(sum_ms_within_trials, max_ms_within_trial);
count_freq = 0;
count2 = 0;
leftmsandcue=0;
leftmsnotcue=0;
rightmsandcue=0;
rightmsnotcue=0;
correct_leftmsandcue=0;
correct_leftmsnotcue=0;
incorrect_leftmsandcue=0;
incorrect_leftmsnotcue=0;
correct_rightmsandcue=0;
correct_rightmsnotcue=0;
incorrect_rightmsandcue=0;
incorrect_rightmsnotcue=0;
TrialType = 0;

for mm = 1:3 %for each trial type
    Trialtype = sprintf('TrialType%d',TrialType);
  for ii = 1:length(vt) 
    if (vt{ii}.MS || vt{ii}.DriftOnly)
        if ((~isfield(vt{ii}, 'trialms_xpositions')) || (isempty(vt{ii}.trialms_xpositions{1}))) %check first block to see if just empty field
            %discard
        elseif vt{ii}.TrialType == TrialType
            temp_len = length(vt{ii}.trialms_xpositions);
            for qq = 1:length(temp_len)
                count2 = count2 +1;
                temp_size = length(vt{ii}.trialms_xpositions{qq});
                ms_matrix_freq(count2,1:temp_size) = vt{ii}.trialms_xpositions{qq};
                %direction
                temp_direction = atan2d(vt{ii}.trialms_ypositions{qq}, vt{ii}.trialms_xpositions{qq});
                %convert t0 0-360 ((3 oclock is 0, and 12 oclock is 90)
                temp_direction = temp_direction + (temp_direction < 0)*360;
                dir(count2) = mean(temp_direction);
                %angle is 90-180 deg and 180-270 (left side)
                %angle is 0-90 deg and 270-360 (right side)
                if ((90 < dir(count2) < 270) && (vt{ii}.Correct==1))
                    if (vt{ii}.ResponseCueLocation == 1 || vt{ii}.ResponseCueLocation == 3)
                    correct_leftmsandcue = correct_leftmsandcue + 1;
                    else 
                    correct_leftmsnotcue = correct_leftmsnotcue+1;
                    end
                elseif ((90 < dir(count2) < 270) && (vt{ii}.Correct==0))
                    if (vt{ii}.ResponseCueLocation == 1 || vt{ii}.ResponseCueLocation == 3)
                    incorrect_leftmsandcue = incorrect_leftmsandcue + 1;
                    else
                    incorrect_leftmsnotcue = incorrect_leftmsnotcue+1;
                    end
                end
                if (((dir(count2) < 90) || (dir(count2) > 270)) && (vt{ii}.Correct==1))
                    if (vt{ii}.ResponseCueLocation == 2 || vt{ii}.ResponseCueLocation == 4)
                    correct_rightmsandcue = correct_rightmsandcue + 1;
                    elseif (vt{ii}.ResponseCueLocation == 1 || vt{ii}.ResponseCueLocation == 3)
                    correct_rightmsnotcue = correct_rightmsnotcue+1;
                    end
                end
                if (((dir(count2) < 90) || (dir(count2) > 270)) && (vt{ii}.Correct==1))
                    if (vt{ii}.ResponseCueLocation == 2 || vt{ii}.ResponseCueLocation == 4)
                    incorrect_rightmsandcue = incorrect_rightmsandcue + 1;
                    elseif (vt{ii}.ResponseCueLocation == 1 || vt{ii}.ResponseCueLocation == 3)
                    incorrect_rightmsnotcue = incorrect_rightmsnotcue+1;
                    end
                end

            end
        end
    end
  end
  
  %count above 0
Above_zero = sum(sum(ms_matrix_freq>0));
%count below 0
Below_zero = sum(sum(ms_matrix_freq<0));
%count total period sum
Compare_all = ~isnan(ms_matrix_freq);
Compare_all = sum(Compare_all(:) == 1);

%print frequency
fprintf('Frequency of left ms: %i\n',...
                Below_zero/Compare_all);
fprintf('Frequency of right ms: %i\n',...
                Above_zero/Compare_all);

Total_ms_with_cues = leftmsandcue+rightmsandcue+leftmsnotcue+rightmsnotcue;
 

%MAYBE SAVE THESE AND COMBINE ACROSS ISISs?
fprintf('PERFORMANCE OF CONGRUENT TRIALS (by ms direction)%i\n')            
%congruent trials (correct)
fprintf('correct_same_ms_dir: %i\n',...
                correct_leftmsandcue+correct_rightmsandcue);
fprintf('incorrect_same_ms_dir: %i\n',...
                incorrect_leftmsandcue+incorrect_rightmsandcue);  
fprintf('correct_diff_ms_dir: %i\n',...
                correct_leftmsnotcue+correct_rightmsnotcue);
fprintf('incorrect_diff_ms_dir: %i\n',...
                incorrect_leftmsnotcue+incorrect_rightmsnotcue);

%congruent trials (incorrect)            
fprintf('correct_leftmsandcue: %i\n',...
                correct_leftmsandcue);
fprintf('correct_rightmsandcue: %i\n',...
                correct_rightmsandcue);
fprintf('incorrect_leftmsandcue: %i\n',...
                incorrect_leftmsandcue);
fprintf('incorrect_rightmsandcue: %i\n',...
                incorrect_rightmsandcue);

%incongruent trials (correct)
fprintf('correct_leftmsnotcue: %i\n',...
                correct_leftmsnotcue);
fprintf('correct_rightmsnotcue: %i\n',...
                correct_rightmsnotcue);
%incongruent trials (incorrect)
fprintf('incorrect_leftmsnotcue: %i\n',...
                incorrect_leftmsnotcue);
fprintf('incorrect_rightmsnotcue: %i\n',...
                incorrect_rightmsnotcue);

     %save all of this data (only valid/invalid)
     Name5 = strcat('PerformanceByDirection_ISI ',number_index, Trialtype);

        MsDetails = struct('correct_same_ms_dir', correct_leftmsandcue+correct_rightmsandcue,...
    'incorrect_same_ms_dir', incorrect_leftmsandcue+incorrect_rightmsandcue,...
    'correct_diff_ms_dir', correct_leftmsnotcue+correct_rightmsnotcue, ...
    'incorrect_diff_ms_dir', incorrect_leftmsnotcue+incorrect_rightmsnotcue, ...
    'correct_leftmsandcue', correct_leftmsandcue, ...
    'correct_rightmsandcue', correct_rightmsandcue, ...
    'incorrect_leftmsandcue', incorrect_leftmsandcue, ...
    'incorrect_rightmsandcue', incorrect_rightmsandcue, ...
    'correct_leftmsnotcue', correct_leftmsnotcue, ...
    'correct_rightmsnotcue', correct_rightmsnotcue, ...
    'incorrect_leftmsnotcue', incorrect_leftmsnotcue, ...
     'incorrect_rightmsnotcue', incorrect_rightmsnotcue, ...
    'Freq_lef_ms', (Below_zero/Compare_all), ...
    'Freq_right_ms', (Above_zero/Compare_all));
    %%%%data above is saved from previous section

eval(strcat(Name5, ' = MsDetails'));

%save all 3 trial type MS details
save(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', name_index,'/Data/', control, '/MsDetails/', Name5,'.mat'), 'MsDetails');           

clear('MsDetails');          

TrialType = TrialType +1;
  
end
           
end
           
close all 
