clear all

subject = 'Karina'; % [ Anna, Adriana, Karina]
control = 'periphery';

if strcmp(subject, 'Adriana')
    ISI4_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/PerformanceByDirection_ISI4TrialType0.mat');
    ISI4_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/PerformanceByDirection_ISI4TrialType1.mat');
    ISI4_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/PerformanceByDirection_ISI4TrialType2.mat');
    ISI6_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/PerformanceByDirection_ISI6TrialType0.mat');
    ISI6_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/PerformanceByDirection_ISI6TrialType1.mat');
    ISI6_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/PerformanceByDirection_ISI6TrialType2.mat');
elseif strcmp(subject, 'Anna')
    ISI4_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/PerformanceByDirection_ISI4TrialType0.mat');
    ISI4_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/PerformanceByDirection_ISI4TrialType1.mat');
    ISI4_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/PerformanceByDirection_ISI4TrialType2.mat');
    ISI6_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/PerformanceByDirection_ISI6TrialType0.mat');
    ISI6_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/PerformanceByDirection_ISI6TrialType1.mat');
    ISI6_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/PerformanceByDirection_ISI6TrialType2.mat');  
elseif strcmp(subject, 'Karina')
    ISI4_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/PerformanceByDirection_ISI4TrialType0.mat');
    ISI4_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/PerformanceByDirection_ISI4TrialType1.mat');
    ISI4_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/PerformanceByDirection_ISI4TrialType2.mat');
    ISI6_0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/PerformanceByDirection_ISI6TrialType0.mat');
    ISI6_1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/PerformanceByDirection_ISI6TrialType1.mat');
    ISI6_2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/PerformanceByDirection_ISI6TrialType2.mat');
end


ISI4_correct_same = (ISI4_0.MsDetails.correct_same_ms_dir + ISI4_1.MsDetails.correct_same_ms_dir + ISI4_2.MsDetails.correct_same_ms_dir);
ISI4_incorrect_same_ms_dir = (ISI4_0.MsDetails.incorrect_same_ms_dir + ISI4_1.MsDetails.incorrect_same_ms_dir + ISI4_2.MsDetails.incorrect_same_ms_dir);
ISI4_same_perf = ISI4_correct_same / (ISI4_correct_same + ISI4_incorrect_same_ms_dir);
ISI4_correct_diff = (ISI4_0.MsDetails.correct_diff_ms_dir + ISI4_1.MsDetails.correct_diff_ms_dir + ISI4_2.MsDetails.correct_diff_ms_dir);
ISI4_incorrect_diff_ms_dir = (ISI4_0.MsDetails.incorrect_diff_ms_dir + ISI4_1.MsDetails.incorrect_diff_ms_dir + ISI4_2.MsDetails.incorrect_diff_ms_dir);
ISI4_diff_perf = ISI4_correct_diff / (ISI4_correct_diff + ISI4_incorrect_diff_ms_dir);

ISI6_correct_same = (ISI6_0.MsDetails.correct_same_ms_dir + ISI6_1.MsDetails.correct_same_ms_dir + ISI6_2.MsDetails.correct_same_ms_dir);
ISI6_incorrect_same_ms_dir = (ISI6_0.MsDetails.incorrect_same_ms_dir + ISI6_1.MsDetails.incorrect_same_ms_dir + ISI6_2.MsDetails.incorrect_same_ms_dir);
ISI6_same_perf = ISI6_correct_same / (ISI6_correct_same + ISI6_incorrect_same_ms_dir);
ISI6_correct_diff = (ISI6_0.MsDetails.correct_diff_ms_dir + ISI6_1.MsDetails.correct_diff_ms_dir + ISI6_2.MsDetails.correct_diff_ms_dir);
ISI6_incorrect_diff_ms_dir = (ISI6_0.MsDetails.incorrect_diff_ms_dir + ISI6_1.MsDetails.incorrect_diff_ms_dir + ISI6_2.MsDetails.incorrect_diff_ms_dir);
ISI6_diff_perf = ISI6_correct_diff / (ISI6_correct_diff + ISI6_incorrect_diff_ms_dir);

[d std_d ci_d crit var_d] = CalculateDprime_2(responsevalue, targetorientation);