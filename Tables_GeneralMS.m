clear all
% make a table with summary of MS statistics
%fovea: Giorgio, Anna, Adriana, Karina
%periphery: Anna, Adriana, Karina

Invalid = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDetails/MSDetails_ISI10TrialType0.mat');
Neutral = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDetails/MSDetails_ISI10TrialType2.mat');
Valid = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/fovea/MsDetails/MSDetails_ISI10TrialType1.mat');


%%
%       z_Rows = {'Drift Span';'Drift Speed';'MS Amp'};
% 
%         ISI_10 = [ E1; E2; z1;s1;p0;h1];
%         ISI_60 = [ E3; E4; z2;s2;p1;h2];
%         ISI_150 = [ E5; E6; z3;s3;p2;h3];
%         ISI_200 = [ E7; E8; z4;s4;p3;h4];
%         ISI_450 = [ E9; E10; z5;s5;p4;h5];
% 
%       T2 = table(ISI_10,ISI_60,ISI_150,ISI_200,ISI_450,'RowNames',z_Rows); 
% 
%      disp(T2)
     
data = [(Invalid.TrialDetails.Driftspan + Neutral.TrialDetails.Driftspan + Valid.TrialDetails.Driftspan)/3; ...
    (Invalid.TrialDetails.Driftspan_SEM + Neutral.TrialDetails.Driftspan_SEM + Valid.TrialDetails.Driftspan_SEM)/3; ...
    (Invalid.TrialDetails.Driftspeed + Neutral.TrialDetails.Driftspeed + Valid.TrialDetails.Driftspeed)/3; ...
    (Invalid.TrialDetails.Driftspeed_SEM + Neutral.TrialDetails.Driftspeed_SEM + Valid.TrialDetails.Driftspeed_SEM)/3;
    (Invalid.TrialDetails.MS_mean_amp + Neutral.TrialDetails.MS_mean_amp + Valid.TrialDetails.MS_mean_amp)/3; 
    (Invalid.TrialDetails.MS_Amp_SEM + Neutral.TrialDetails.MS_Amp_SEM + Valid.TrialDetails.MS_Amp_SEM)/3]; 

f=figure()     
 % Create the column and row names in cell arrays 
cnames = {'Invalid','Neutral','Valid'};
rnames = {'Drift Span';'Drift Span Error';'Drift Speed';'Drift Speed Error';...
    'MS Amp';'MS Amp Error'};
% Create the uitable
t = uitable(f,'Data',data,...
            'ColumnName',cnames,... 
            'RowName',rnames,...
            'ColumnWidth',{50})
subplot(1,1,1),plot(1)
pos = get(subplot(1,1,1),'position');
delete(subplot(2,1,2))
set(t,'units','normalized')
set(t,'position',pos)

