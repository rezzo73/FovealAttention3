% Combining ms frequency graphs (periphery)
clear all

control = 'periphery'; %only do periphery
subject = 'Anna'; %['Adriana', 'Anna', 'Karina', 'Giorgio'];
%HAVE TO DO KARINA FOVEA MANUALLY B/C NO MS FOR CERTAIN CONDITIONS.

if strcmp(control, 'periphery')
    if strcmp(subject, 'Adriana')
        ISI5_TT0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/freq_graph_ISI5TrialType0.mat');
        ISI5_TT1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/freq_graph_ISI5TrialType1.mat');
        ISI5_TT2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/freq_graph_ISI5TrialType2.mat');
        ISI60_TT0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/freq_graph_ISI60TrialType0.mat');
        ISI60_TT1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/freq_graph_ISI60TrialType1.mat');
        ISI60_TT2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/freq_graph_ISI60TrialType2.mat');
        ISI450_TT0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/freq_graph_ISI450TrialType0.mat');
        ISI450_TT1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/freq_graph_ISI450TrialType1.mat');
        ISI450_TT2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/freq_graph_ISI450TrialType2.mat');
        ISI750_TT0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/freq_graph_ISI750TrialType0.mat');
        ISI750_TT1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/freq_graph_ISI750TrialType1.mat');
        ISI750_TT2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Adriana/Data/periphery/MsDetails/freq_graph_ISI750TrialType2.mat');
    elseif strcmp(subject, 'Anna')
        ISI5_TT0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/freq_graph_ISI5TrialType0.mat');
        ISI5_TT1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/freq_graph_ISI5TrialType1.mat');
        ISI5_TT2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/freq_graph_ISI5TrialType2.mat');
        ISI60_TT0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/freq_graph_ISI60TrialType0.mat');
        ISI60_TT1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/freq_graph_ISI60TrialType1.mat');
        ISI60_TT2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/freq_graph_ISI60TrialType2.mat');
        ISI450_TT0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/freq_graph_ISI450TrialType0.mat');
        ISI450_TT1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/freq_graph_ISI450TrialType1.mat');
        ISI450_TT2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/freq_graph_ISI450TrialType2.mat');
        ISI750_TT0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/freq_graph_ISI750TrialType0.mat');
        ISI750_TT1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/freq_graph_ISI750TrialType1.mat');
        ISI750_TT2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Anna/Data/periphery/MsDetails/freq_graph_ISI750TrialType2.mat');    
    elseif strcmp(subject, 'Karina')
        ISI5_TT0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/freq_graph_ISI5TrialType0.mat');
        ISI5_TT1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/freq_graph_ISI5TrialType1.mat');
        ISI5_TT2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/freq_graph_ISI5TrialType2.mat');
        ISI60_TT0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/freq_graph_ISI60TrialType0.mat');
        ISI60_TT1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/freq_graph_ISI60TrialType1.mat');
        ISI60_TT2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/freq_graph_ISI60TrialType2.mat');
        ISI450_TT0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/freq_graph_ISI450TrialType0.mat');
        ISI450_TT1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/freq_graph_ISI450TrialType1.mat');
        ISI450_TT2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/freq_graph_ISI450TrialType2.mat');
        ISI750_TT0 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/freq_graph_ISI750TrialType0.mat');
        ISI750_TT1 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/freq_graph_ISI750TrialType1.mat');
        ISI750_TT2 = load('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/Karina/Data/periphery/MsDetails/freq_graph_ISI750TrialType2.mat');   
    end
end

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%% periphery
if strcmp(control, 'periphery')

    trialtype = 0;

    for ii = 1:3
    %valid
    if trialtype == 1
        %ISI5 = ISI5_TT1.freq_graph;
        %ISI60 = ISI60_TT1.freq_graph;
        ISI450 = ISI450_TT1.freq_graph;
        ISI750 = ISI750_TT1.freq_graph;
    end

    %invalid
    if trialtype == 0
        %ISI5 = ISI5_TT0.freq_graph;
        %ISI60 = ISI60_TT0.freq_graph;
        ISI450 = ISI450_TT0.freq_graph;
        ISI750 = ISI750_TT0.freq_graph;
    end

    %neutral
    if trialtype == 2
        %ISI5 = ISI5_TT2.freq_graph;
        %ISI60 = ISI60_TT2.freq_graph;
        ISI450 = ISI450_TT2.freq_graph;
        ISI750 = ISI750_TT2.freq_graph;
    end

    %normalize based on max # of ms:
    max_450 = max(ISI450);
    max_750 = max(ISI750);
    
    ISI450 = ISI450./max_450;
    ISI750 = ISI750./max_750;
    
    max_trial_length = 3000;

    %plot binary frequency of ms over trial time
    figure()
    ylim([0 50])
    %axis([0 3000 0 (max(sum(out2))+5)])
    %plot(ISI5(:,1:max_trial_length),'m','LineWidth',1.5)
    %hold on
    %plot(ISI60(:,1:max_trial_length),'r','LineWidth',1.5)
    %hold on
    plot(ISI450(:,1:max_trial_length),'g','LineWidth',1.5)
    hold on
    plot(ISI750(:,1:max_trial_length),'b','LineWidth',1.5)
    hold on 
    vline(500,'k:') %time cue on
    hold on
    vline(500+40+750+100+100,'b:') %time cue on
    hold on
    %vline(500+40+5+100+100,'m:') %time cue on
    %hold on
    %vline(500+40+60+100+100,'r:') %time cue on
    %hold on
    vline(500+40+450+100+100,'g:') %time cue on
    legend('ISI450','ISI750')
    %legend('ISI5','ISI60','ISI450','ISI750')
    %vline(ceil(ave_E))
    title(sprintf('Normalized Frequency of microsaccade per milisecond for trial type %d', trialtype))
    ylabel('amount of trials')
    xlabel('milisecond')

    saveas(gcf, sprintf(strcat('/Volumes/APLAB/Rania_Projects_BACK_UP/FovealAttention3/Data_Subjects/', subject,'/Images/', control, '/MsDetails/', ...
              sprintf('TrialType%d', trialtype),'Normalized_ISI_Freq.jpg')));

    trialtype = trialtype+1;

    end
end

%close all
clear all