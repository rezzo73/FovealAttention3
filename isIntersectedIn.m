% This function checks if a single event intersects one of the events 
% specified in the list. If Duration is equal to 0, the function 
% checks that the single instant of time is included in one
% of the events in the list.
%
% Syntax:
%   [Answer, Intersected] = isIntersectedIn(Start, Events)
%   [Answer, Intersected] = isIntersectedIn(Start, Duration, Events)
%
% Start      : Position in time of the event (in ms)
% Duration   : Duration of the event (in ms)
% Events     : Structure containing the list of events
%
% Answer     : TRUE if the single event intersects one of the events
% Intersected: Resulting vector that contains the position of the events
%              that intersect the specified period
%
function [Answer, Intersected] = isIntersectedIn(varargin)

  % Verify which parameters format has been passed
  if nargin < 2, p_ematError(4, 'isIntersectedIn'); end

  % Initialize missing parameters
  Duration = 1; 
  if nargin == 3, Duration = varargin{2};  end
  if ~isaEvent(varargin{end}), p_ematError(6, 'isIntersectedIn'); end

  Start = varargin{1};
  Events = varargin{end};

  Intersected = [];

  % Check that if the single event is completely 
  % included in one of the events of the list
  for i = 1:countEvents(Events)
    if (Start >= Events.start(i) && ...
        Start < Events.start(i) + Events.duration(i)) || ...
       (Start <= Events.start(i) && ...
        Start + Duration >= Events.start(i) + Events.duration(i)) || ...
       (Start + Duration > Events.start(i) && ...
        Start + Duration <= Events.start(i) + Events.duration(i))
      Intersected(end + 1) = i; %#ok<AGROW>
    end
  end

  % Check if one of the events intersects the specified period
  Answer = ~isempty(Intersected);
end